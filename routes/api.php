<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Orion\Facades\Orion;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
//Client API'S


    // our routes to be protected will go in here
//GetCategories
Route::get('/categories', 'API\CategoryController@index' );

Route::get('/category/{id}', 'API\CategoryController@getCategoryProducts');
//getFeaturedCategories
Route::get('/getFeaturedCategories', 'API\CategoryController@getFeaturedCategories');
//GetSubCategories
Route::get('/subcategories', 'API\SubCategoryController@index');
//GetProducts
Route::get('/products', 'API\ProductController@index');

Route::get('/product/{id}', 'API\ProductController@getProduct');
//last products 6
Route::get('/getLastProducts', 'API\ProductController@getLastProducts');
//related products
Route::get('/getRelatedProducts/{subcategory_id}/{id}', 'API\ProductController@getRelatedProducts');
//related with category
Route::get('/getCategorizedProducts/{id}', 'API\ProductController@getCategorizedProducts');
//Add Comment
Route::post('/AddComment', 'API\ShopController@AddComment');
//Change Password
Route::post('/ChangePassword', 'API\ProfileController@changePassword')->middleware("auth:api");
//GetSizes
Route::get('/sizes', 'API\SizeController@index');
//register
Route::post('/register', 'API\AuthController@AddUser');
//login
Route::post('/login', 'API\AuthController@Login');
//logout
Route::get('/logout', 'API\AuthController@logout');
//feedbacks
Route::post('/addFeedback', 'API\FeedbackController@AddFeedback');
//wishlistSection
Route::middleware(['auth:api'])->group(function () {
    Route::get('/wishlist', 'API\WishlistController@GetMyWishlist');

    Route::get('/removewishlist/{id}', 'API\WishlistController@RemoveFromWishlist');

    Route::post('/AddToWishlist', 'API\WishlistController@AddToWishlist');

});


//Contact Api
Route::post('/contactus', 'Web\ContactUsController@addFeedback')->name('addcontactus');
