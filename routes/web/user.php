<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/main', 'HomeController@mainPage')->name('main');

Auth::routes();
Route::get('/admin', 'HomeController@index')->name('home')->middleware('admin');



Route::get('/contactus', 'Web\ContactUsController@index')->name('contactus');
Route::post('/contactus', 'Web\ContactUsController@addFeedback');

Route::get('/aboutus', 'Web\AboutUsController@index')->name('aboutus');

Route::get('/shop', 'Web\ShopController@index')->name('shop');
Route::get('/filterProducts', 'Web\ProductController@filterProducts')->name('filterProducts');


Route::get('/productDetails/{id}', 'Web\ShopController@productDetails')->name('productDetails');


Route::get('/categoryProducts/{id}', 'Web\ProductController@GetCategorizedProducts')->name('GetCategorizedProducts');

Route::get('/subcategoryProducts/{id}', 'Web\ProductController@GetSubCategorizedProducts')->name('GetSubCategorizedProducts');


Route::get('/paypal', ['as' => 'payment', 'uses' => 'Web\PaypalController@payWithpaypal']);

Route::get('/paypal/status',['as' => 'status', 'uses' => 'Web\PaypalController@getPaymentStatus']);

Route::group(['middleware' => 'auth'], function () {
    Route::post('/AddToWishList', 'Web\WishlistController@AddToWishList')->name('wishlist');

    Route::get('/Wishlist', 'Web\WishlistController@index');

    Route::get('/RemoveFromWishList/{product_id}', 'Web\WishlistController@RemoveFromWishList')->name('RemoveFromWishList');

    Route::post('/WishlistToCart', 'Web\WishlistController@WishlistToCart')->name('WishlistToCart');

    Route::post('/ApplyCoupon', 'Web\CartController@ApplyCoupon')->name('ApplyCoupon');

    Route::get('/profile', 'Web\ProfileController@index')->name('profile');

    Route::post('/changePassword', 'Web\ProfileController@changePassword')->name('changePassword');

    Route::post('/checkOrder', 'Web\ProfileController@checkOrder')->name('checkOrder');

    Route::get('/returnOrder/{id}', 'Web\ProfileController@returnOrder')->name('returnOrder');
});




Route::post('/AddComment', 'Web\ShopController@AddComment')->name('AddComment');

Route::post('/Subscribe', 'Web\SubscribeController@store')->name('AddEmail');


Route::group(['middleware' => 'auth'], function () {
    Route::post('/AddToCart', 'Web\CartController@AddToCart')->name('AddToCart');

    Route::get('/RemoveFromCart/{id}', 'Web\CartController@RemoveFromCart')->name('RemoveFromCart');

    Route::get('/Cart', 'Web\CartController@index')->name('Cart');

    Route::get('/ClearCart', 'Web\CartController@ClearCart')->name('ClearCart');

    Route::get('/Checkout', 'Web\CartController@checkout')->name('checkout');

    Route::post('/Finalcheckout', 'Web\CartController@finalcheckout')->name('finalcheckout');

});

Route::get('/payment', 'Web\PaymentController@index')->name('PaymentProccess');

Route::post('/payment', 'Web\PaymentController@PaymentProcess');



//Facebook Registering And Login
Route::get('/redirect', 'SocialAuthFacebookController@redirect');

Route::get('/callback', 'SocialAuthFacebookController@callback');
//Google Registering And Login
Route::get('/redirects', 'SocialAuthGoogleController@redirect');

Route::get('/callbacks', 'SocialAuthGoogleController@callback');


