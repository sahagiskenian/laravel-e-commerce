<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 /* ADMIN SECTION

*/
Route::prefix('admin')->middleware(['auth','admin'])->group(function () {

    Route::middleware('editor')->group(function () {
    //Categories Section
    Route::get('categories', 'Admin\CategoryController@index' );
    Route::post('categories', 'Admin\CategoryController@store' );
    Route::get('RemoveCategory/{id}', 'Admin\CategoryController@RemoveCategory' )->name("deletecategory");

    //SubCategories Section
    Route::get('subcategories', 'Admin\SubCategoryController@index' );
    Route::post('subcategories', 'Admin\SubCategoryController@store' );
    Route::get('RemoveSubCategory/{id}', 'Admin\SubCategoryController@RemoveSubCategory' )->name("deletesubcategory");

    //Size Section
    Route::get('sizes', 'Admin\SizeController@index' );
    Route::post('sizes', 'Admin\SizeController@store' );
    Route::get('RemoveSize/{id}', 'Admin\SizeController@RemoveSize' )->name("deletesize");

    Route::get('addproduct', 'Admin\ProductController@index' )->name("addproduct");
    Route::post('storeProduct', 'Admin\ProductController@storeProduct' )->name("storeProduct");

    Route::get('subcategory/{id}', 'Admin\ProductController@loadsubCategories' );
    Route::get('products/{id}', 'Admin\ProductController@products' );
    Route::get('deleteproduct/{id}', 'Admin\ProductController@deleteproduct' )->name('deleteproduct');
    Route::get('hideProduct/{id}', 'Admin\ProductController@hideProduct' )->name('hideProduct');
    Route::get('topRatedProduct/{id}', 'Admin\ProductController@topRatedProduct' )->name('topRatedProduct');
    Route::get('EditProduct/{id}', 'Admin\ProductController@EditProduct' )->name('EditProduct');
    Route::get('deleteproductImage/{id}', 'Admin\ProductController@deleteproductImage' )->name('deleteproductImage');
    Route::get('ProductMin', 'Admin\ProductController@ProductMin' )->name("ProductMin");

    Route::get('feedbacks', 'Admin\FeedbacksController@index');
    Route::get('RemoveFeedback/{id}', 'Admin\FeedbacksController@RemoveFeedback' )->name("deletefeedback");


    Route::get('orders/{id}', 'Admin\OrderController@index')->name("orders");


    Route::get('orderDetails/{id}', 'Admin\OrderController@orderDetails');

    Route::get('AcceptOrder/{id}', 'Admin\OrderController@AcceptPayment');
    Route::get('RefuseOrder/{id}', 'Admin\OrderController@RefuseOrder');
    Route::get('ProcessOrder/{id}', 'Admin\OrderController@ProcessOrder');
    Route::get('DeliverOrder/{id}', 'Admin\OrderController@DeliverOrder');

    Route::get('PendingReturnOrder', 'Admin\OrderController@PendingReturnOrder')->name("PendingReturnOrder");
    Route::get('ReturnedOrder', 'Admin\OrderController@ReturnedOrders')->name("ReturnedOrder");
    Route::get('acceptReturn/{id}', 'Admin\OrderController@ReturnOrder')->name("acceptReturn");


    Route::get('Statistics', 'Admin\StatisticsController@index')->name("Statistics");


    Route::get('contactSettings', 'Admin\SettingController@contactSettings')->name("contactSettings");

    Route::post('EditContactSettings', 'Admin\SettingController@EditContactSettings')->name("EditContactSettings");

    Route::get('aboutusStats', 'Admin\SettingController@aboutusStats')->name("aboutusStats");

    Route::post('aboutusEdit', 'Admin\SettingController@aboutusEdit')->name("aboutusEdit");

    Route::get('mainpageSetting', 'Admin\SettingController@mainpageSetting')->name("mainpageSetting");
    Route::post('editmainpageSetting', 'Admin\SettingController@editmainpageSetting')->name("editmainpageSetting");


    Route::get('services', 'Admin\ServiceController@index' );
    Route::post('services', 'Admin\ServiceController@create' );
    Route::get('removeservice/{id}', 'Admin\ServiceController@destroy' )->name("removeservice");

});


    Route::middleware('reporter')->group(function () {
            //Reports
    Route::get('TodayOrder', 'Admin\ReportController@TodayOrder');
    Route::get('TodayDelivery', 'Admin\ReportController@TodayDelivery');
    Route::get('ThisMonth', 'Admin\ReportController@ThisMonth');
    Route::get('Search', 'Admin\ReportController@Search');

    Route::post('SearchByYear', 'Admin\ReportController@SearchByYear');
    Route::post('SearchByMonth', 'Admin\ReportController@SearchByMonth');
    Route::post('SearchByDate', 'Admin\ReportController@SearchByDate');
   });



 Route::middleware('mainAdmin')->group(function () {
     //admins Section
 Route::get('admins', 'Admin\UserController@index' )->name("admins");
 Route::post('admins', 'Admin\UserController@addAdmin' )->name("addAdmins");
 Route::get('RemoveAdmin/{id}', 'Admin\UserController@RemoveAdmin' )->name("deleteuser");
// users Section

 Route::get('users', 'Admin\UserController@indexUsers' )->name("users");

 Route::get('RemoveUser/{id}', 'Admin\UserController@RemoveUser' )->name("deleteuserr");
});

});
