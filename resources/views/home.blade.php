 @php
 $categories= \App\category::all()
 @endphp
<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
  <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
  <meta name="author" content="PIXINVENT">
  <title>My ECommerce Dashboard
  </title>
  @include('layouts.adminlayouts.adminstyles')
</head>
<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar"
data-open="click" data-menu="vertical-menu" data-col="2-columns">
@include('sweetalert::alert')
  <!-- fixed-top-->
 @include('layouts.adminlayouts.adminNavbar')
  @yield('content')
  <!-- ////////////////////////////////////////////////////////////////////////////-->
 @include("layouts.adminlayouts.adminFooter")
  <!-- BEGIN VENDOR JS-->


@include('layouts.adminlayouts.adminscripts')
  <!-- END PAGE LEVEL JS-->
  @yield('externalscripts')
</body>
</html>
