@extends('home')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title"> Admin's  </h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>
        <div class="content-header-right  offset-3 col-md-3 col-12">
            <button type="button" class="btn btn-outline-primary block btn-lg" data-toggle="modal"
            data-target="#default">
             Add Admin
            </button>
            <!-- Modal -->
            <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
            aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Admin  </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                 <form action="{{route('addAdmins')}}" method="POST">
                    @csrf
                    <fieldset class="form-group">
                        <input name="name" placeholder="Name" type="text" class="form-control" id="basicInput">
                      </fieldset>
                      <fieldset class="form-group">
                        <input name="email" placeholder="email" type="email" class="form-control" id="basicInput">
                      </fieldset>
                      <fieldset class="form-group">
                        <input name="password" placeholder="password" type="password" class="form-control" id="basicInput">
                      </fieldset>
                      <fieldset class="form-group">
                      <label for="exampleFormControlSelect1"> Admin Type  </label>
                      <select class="form-control" name="adminType" id="exampleFormControlSelect1">
                        <option value="1" selected>Main Admin</option>
                        <option value="2">Editor</option>
                        <option value="3">Reporter</option>

                      </select>
                    </fieldset>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-primary">Add</button>
                </form>
                  </div>
                </div>
              </div>
            </div>


        @foreach ($users as $user)
        <div class="modal fade text-left" id="s{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="#s{{$user->id}}"
        aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Admins  </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
             <form action="{{route('addAdmins')}}" method="POST">
                @csrf
                <fieldset class="form-group">
                    <input name="name" placeholder="Name" value="{{$user->name}}" type="text" class="form-control" id="basicInput">
                  </fieldset>
                  <fieldset class="form-group">
                    <input name="email" placeholder="email" value="{{$user->email}}" type="email" class="form-control" id="basicInput">
                  </fieldset>
                  <fieldset class="form-group">
                    <input name="password" placeholder="password" type="password" class="form-control" id="basicInput">
                  </fieldset>
                  <fieldset class="form-group">
                    <input hidden name="id" placeholder="password" value="{{$user->id}}" type="password" class="form-control" id="basicInput">
                  </fieldset>
                  <fieldset class="form-group">
                    <label for="exampleFormControlSelect1"> Admin Type  </label>
                    <select class="form-control" name="adminType" id="exampleFormControlSelect1">
                        <option value="{{$user->adminType}}" selected> </option>
                      <option value="1">Main Admin</option>
                      <option value="2">Editor</option>
                      <option value="3">Reporter</option>

                    </select>
                  </fieldset>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-outline-primary">Edit</button>
            </form>
              </div>
            </div>
          </div>
        </div>
        @endforeach
        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Users</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                            <th>#</th>
                          <th>  Name</th>
                          <th>  Email</th>
                          <th>  Type</th>
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
<td> {{$user->id}} </td>
<td> {{$user->name}} </td>
<td> {{$user->email}} </td>
@if ($user->adminType==1)
<td> Main </td>
@elseif ($user->adminType==2)
<td> Editor </td>
@elseif ($user->adminType==3)
<td> Reporter </td>
@endif

<td> <button type="button" data-toggle="modal"
data-target="#s{{$user->id}}" class="btn btn-primary"> Edit </button> </td>
<td> <a href="{{route('deleteuser',['id'=>$user->id])}}" class="btn btn-danger"> Delete </a> </td>
                            </tr>
                        @endforeach

                    </tbody>


                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
@endsection
@section('externalscripts')
<script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
type="text/javascript"></script>
@endsection
