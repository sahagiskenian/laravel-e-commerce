@extends('home')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
        <h3 class="content-header-title">Products</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>
        <div class="content-header-right  offset-3 col-md-3 col-12">

            <!-- Modal -->
            <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
            aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Category  </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                 <form action="categories" method="POST">
                    @csrf
                    <fieldset class="form-group">
                        <input name="category_name" placeholder="Category name" type="text" class="form-control" id="basicInput">
                      </fieldset>



                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-primary">Add</button>
                </form>
                  </div>
                </div>
              </div>
            </div>



        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Product</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>

                          <th>Name</th>
                          <th>Category</th>
                          <th>Code</th>
                          <th>Discount</th>

                          <th>Edit</th>
                          <th>Delete</th>
                          <th>Hide</th>
                          <th>Top Rated</th>
                        </tr>
                      </thead>
                    <tbody>
                        @foreach ($products as $product)
                            <tr>

<td> {{$product->product_name}} </td>
<td> {{$product->category->category_name}} </td>
<td> {{$product->product_code}} </td>
<td> @if ($product->discount )
    {{$product->discount}}
@else
    0
@endif


</td>

<td> <a   href="{{route('EditProduct',['id'=>$product->id])}}"  class="btn btn-primary"> Edit </a> </td>
<td> <a href="{{route('deleteproduct',['id'=>$product->id])}}" class="btn btn-danger"> Delete </a> </td>

@if($product->hide==0)
<td> <a href="{{route('hideProduct',['id'=>$product->id])}}" class="btn btn-secondary"> Hide </a> </td>
 @else
 <td> <a href="{{route('hideProduct',['id'=>$product->id])}}" class="btn btn-secondary"> Appear </a> </td>
@endif
@if($product->top_rated==0)
<td> <a href="{{route('topRatedProduct',['id'=>$product->id])}}" class="btn btn-success"> Top Rated </a> </td>
 @else
 <td> <a href="{{route('topRatedProduct',['id'=>$product->id])}}" class="btn btn-success"> Remove   </a> </td>

@endif</tr>
                        @endforeach

                    </tbody>


                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
@endsection
@section('externalscripts')
<script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
type="text/javascript"></script>
@endsection
