

@extends('home')





@section('content')


 <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">Products</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>

      </div>
       <div class="content-body">
        <!-- Basic Inputs start -->
        <section class="basic-inputs">
          <div class="row match-height">
            <div class="col-xl-12 col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Add Product</h4>
                </div>
                <div class="card-block">
                  <div class="card-body">
                      <form method="POST" action="/admin/storeProduct"  enctype="multipart/form-data">
                        @csrf
                    <fieldset class="form-group  ">
                      <input type="text" placeholder="Product Name" name="product_name" class="form-control" id="basicInput">
                    </fieldset>
                    <fieldset class="form-group">
                        <textarea class="form-control" placeholder="Product Description" name="product_description" id="basicTextarea" rows="3"></textarea>
                      </fieldset>
                    <fieldset class="form-group  ">
                      <input type="number" placeholder="Quantity" name="quantity" class="form-control" id="basicInput">
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="text" placeholder="Price" name="price" class="form-control" id="basicInput">
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="text" placeholder="discount" name="discount" class="form-control" id="basicInput">
                    </fieldset>

                    <fieldset class="form-group">
                        <input type="text" placeholder="product_code" name="product_code" class="form-control" id="basicInput">
                    </fieldset>
                    <fieldset class="form-group">
                      <select class="custom-select" name="category_id" id="category_select">
                        <option selected>Open this select menu</option>
                        @foreach ($categories as $category)
                      <option value="{{$category->id}}">{{$category->category_name}}</option>
                        @endforeach


                      </select>
                    </fieldset>
                    <fieldset class="form-group">
                        <select class="custom-select" name="subcategory_id" id="subcategory_select">




                        </select>
                      </fieldset>
                    <fieldset class="form-group">
                      <select class="form-control" name="sizes[]" id="countrySelect" multiple="multiple">

                         @foreach ($sizes as $size)
                         <option value="{{$size->id}}">{{$size->size_name}}</option>
                         @endforeach
                      </select>
                    </fieldset>

          <fieldset class="form-group">
            <div class="custom-file">
              <input type="file" name="main_image" class="custom-file-input" id="inputGroupFile01">
              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
            </div>
          </fieldset>
          <fieldset class="form-group">
            <div class="custom-file">
              <input type="file" name="images[]" class="custom-file-input" id="inputGroupFile01" multiple>
              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
            </div>
          </fieldset>
<button class="btn btn-primary" type="submit" > Add Product </button>

        </form>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </section>
        <!-- Basic Inputs end -->
        <!-- Inputs Type Options start -->


        <!-- Control Color end -->
       </div>
    </div>
  </div>



  @endsection

  @section('externalscripts')
  <script type="text/javascript">
  $(document).ready(function(){
    var i=0;
   $("#category_select").on('change',function(e){

var value=e.target.value;

       $.ajax({
           url:'subcategory/'+value,
           type:'get',
           dataType:'json',




           success:function(response){
               if(response[0].length==0){
                $("#subcategory_select").empty();
                var _html= `    <option selected>Open this select menu</option> `
    $("#subcategory_select").append(_html);

               }
               else {
console.log(response);

$.each(response[0],function(index,value){

        var _html= `     <option value="${value.id}">${value.subcategory_name}</option> `
    $("#subcategory_select").append(_html);
console.log("s");
                 });



               }
             }


       });
   });











  });


</script>
  @endsection
