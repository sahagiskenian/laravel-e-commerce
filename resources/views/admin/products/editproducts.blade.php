

@extends('home')





@section('content')


 <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">Products</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>

      </div>
       <div class="content-body">
        <!-- Basic Inputs start -->
        <section class="basic-inputs">
          <div class="row match-height">
            <div class="col-xl-12 col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Edit Product</h4>
                </div>
                <div class="card-block">
                  <div class="card-body">
                      <form method="POST" action="/admin/storeProduct"  enctype="multipart/form-data">
                        @csrf
                    <fieldset class="form-group  ">
                    <input type="text" placeholder="Product Name" value="{{$product->product_name}}" name="product_name" class="form-control" id="basicInput">
                    </fieldset>
                    <fieldset class="form-group">
                        <textarea class="form-control" placeholder="Product Description"   name="product_description" id="basicTextarea" rows="3">{{$product->product_description}}</textarea>
                      </fieldset>
                    <fieldset class="form-group  ">
                      <input type="number" placeholder="Quantity" value="{{$product->quantity}}" name="quantity" class="form-control" id="basicInput">
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="text" placeholder="Price" value="{{$product->price}}" name="price" class="form-control" id="basicInput">
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="text" placeholder="discount" value="{{$product->discount}}" name="discount" class="form-control" id="basicInput">
                    </fieldset>

                    <fieldset class="form-group">
                        <input type="text" placeholder="product_code" value="{{$product->product_code}}" name="product_code" class="form-control" id="basicInput">
                    </fieldset>
                    <fieldset class="form-group">
                      <select class="custom-select" name="category_id" id="category_select">
                        <option selected>Open this select menu</option>
                        @foreach ($categories as $category)
                                                    @if($category->id == $product->category_id)

                                                    <option value="{{$category->id}}" selected>{{$category->category_name}}</option>

                                                    @else
                                                    <option value="{{$category->id}}"  >{{$category->category_name}}</option>
                                                    @endif
                                                    @endforeach

                      </select>
                    </fieldset>
                    <fieldset class="form-group">
                        <select class="custom-select" name="subcategory_id" id="subcategory_select">

                            @foreach ($subcategories as $subcategory)
                            @if($subcategory->id == $product->subcategory_id)

                            <option value="{{$subcategory->id}}" selected>{{$subcategory->subcategory_name}}</option>

                            @else
                            <option value="{{$subcategory->id}}"  >{{$subcategory->subcategory_name}}</option>
                            @endif
                            @endforeach


                        </select>
                      </fieldset>
                    <fieldset class="form-group">
                      <select class="form-control" name="sizes[]" id="countrySelect" multiple="multiple">
                          {{$product->sizes }}
                          @foreach ($sizes as $size)
                          <label class="form-check-label"  >
                            <option value="{{$size->id}}"

                          @foreach ($product->sizes as $psize)
                          @if ($size->id == $psize->id)
                              selected
                          @endif
                          @endforeach
                           >  {{$size->size_name}}</option><br>
                          @endforeach


                      </select>
                    </fieldset>

          <fieldset class="form-group">
            <div class="custom-file">
              <input type="file" name="main_image" class="custom-file-input" id="inputGroupFile01">
              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>


              <img   width="200" height="200" src="{{asset($product->main_image)}}" />
            </div>
          </fieldset>
          <fieldset class="form-group">
            <div class="custom-file">
              <input type="file" name="images[]" class="custom-file-input" id="inputGroupFile01" multiple>
              <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
            </div>
          </fieldset>
          <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Image</th>
                <th scope="col">Delete </th>

              </tr>
            </thead>
            <tbody>

                  @foreach ($product->productImages as $image)

                  <tr>
              <th scope="row">{{$image->id}}</th>
                  <td><img   width="200" height="200" src="{{asset($image->product_image)}}" /></td>
                  <td><a href="{{route('deleteproductImage',['id'=> $image->id ])}}" class="btn btn-danger"> Delete </a></td>
                </tr>
                  @endforeach




            </tbody>
          </table>
        <input name="id" type="text" value="{{$product->id}}"   hidden    />
<button class="btn btn-primary" type="submit" > Edit Product </button>

        </form>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </section>
        <!-- Basic Inputs end -->
        <!-- Inputs Type Options start -->


        <!-- Control Color end -->
       </div>
    </div>
  </div>



  @endsection


