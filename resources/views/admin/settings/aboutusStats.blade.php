

@extends('home')





@section('content')


 <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">About Us </h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>

      </div>
       <div class="content-body">
        <!-- Basic Inputs start -->
        <section class="basic-inputs">
          <div class="row match-height">
            <div class="col-xl-12 col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">About Stats and Information</h4>
                </div>
                <div class="card-block">
                  <div class="card-body">
                      <form method="POST" action="/admin/aboutusEdit"  enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                    <fieldset class="form-group col-md-3  ">
                      <input type="text" placeholder="About Stats1" value="{{$setting->aboutus_stats1}}" name="aboutus_stats1" class="form-control" id="basicInput">
                    </fieldset>
                    <fieldset class="form-group col-md-3  ">
                        <input type="text" placeholder="About Stats2" value="{{$setting->aboutus_stats2}}" name="aboutus_stats2" class="form-control" id="basicInput">
                      </fieldset>
                      <fieldset class="form-group col-md-3  ">
                        <input type="text" placeholder="About Stats3" value="{{$setting->aboutus_stats3}}" name="aboutus_stats3" class="form-control" id="basicInput">
                      </fieldset>
                      <fieldset class="form-group col-md-3  ">
                        <input type="text" placeholder="About Stats4" value="{{$setting->aboutus_stats4}}" name="aboutus_stats4" class="form-control" id="basicInput">
                      </fieldset>
                        </div>


                   <fieldset class="form-group  ">
                    <input type="text" placeholder="about us title" value="{{$setting->aboutus_title}}" name="aboutus_title" class="form-control" id="basicInput">
                  </fieldset>
                    <fieldset class="form-group">
                        <textarea class="form-control"   placeholder="About us description" name="aboutus_description" id="basicTextarea" rows="3">{{$setting->aboutus_description}}</textarea>
                      </fieldset>

                    <fieldset class="form-group">
                        <input type="text" placeholder="Facebook Url" value="{{$setting->facebook_url}}" name="facebook_url" class="form-control" id="basicInput">
                    </fieldset>
                    <div class="row">
                        <fieldset class="form-group col-md-3  ">
                          <input type="text" placeholder="About num1" value="{{$setting->aboutus_num1}}" name="aboutus_num1" class="form-control" id="basicInput">
                        </fieldset>
                        <fieldset class="form-group col-md-3  ">
                            <input type="text" placeholder="About num2" value="{{$setting->aboutus_num2}}" name="aboutus_num2" class="form-control" id="basicInput">
                          </fieldset>
                          <fieldset class="form-group col-md-3  ">
                            <input type="text" placeholder="About num3" value="{{$setting->aboutus_num3}}" name="aboutus_num3" class="form-control" id="basicInput">
                          </fieldset>
                          <fieldset class="form-group col-md-3  ">
                            <input type="text" placeholder="About num4" value="{{$setting->aboutus_num4}}" name="aboutus_num4" class="form-control" id="basicInput">
                          </fieldset>
                            </div>
                            <fieldset class="form-group">
                                <div class="custom-file">
                                     <input type="file" name="aboutus_image" class="custom-file-input" id="inputGroupFile01">
                                     <label class="custom-file-label" for="inputGroupFile01">Choose Image Aboutus</label>
                               </div>
                               <img class="img-fluid"  src="{{asset($setting->aboutus_image)}}"   />
                      </fieldset>


<button class="btn btn-primary" type="submit" > Edit About Us </button>

        </form>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </section>
        <!-- Basic Inputs end -->
        <!-- Inputs Type Options start -->


        <!-- Control Color end -->
       </div>
    </div>
  </div>



  @endsection


