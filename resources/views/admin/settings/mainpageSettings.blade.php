

@extends('home')





@section('content')


 <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">Main Page </h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>

      </div>
       <div class="content-body">
        <!-- Basic Inputs start -->
        <section class="basic-inputs">
          <div class="row match-height">
            <div class="col-xl-12 col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Main Page Information</h4>
                </div>
                <div class="card-block">
                  <div class="card-body">
                      <form method="POST" action="/admin/editmainpageSetting"  enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                    <fieldset class="form-group col-md-6  ">
                      <input type="text" placeholder="Slider 1 Text" value="{{$setting->slider1_text}}" name="slider1_text" class="form-control" id="basicInput">
                    </fieldset>
                    <fieldset class="form-group col-md-6  ">
                        <input type="text" placeholder="Slider 2 Text" value="{{$setting->slider2_text}}" name="slider2_text" class="form-control" id="basicInput">
                      </fieldset>
                      <fieldset class="form-group col-md-6  ">
                        <input type="text" placeholder="Slider 3 Text" value="{{$setting->slider3_text}}" name="slider3_text" class="form-control" id="basicInput">
                      </fieldset>

                        </div>
                            <fieldset class="form-group">
                                <div class="custom-file">
                                     <input type="file" name="slider_image1" class="custom-file-input" id="inputGroupFile01">
                                     <label class="custom-file-label" for="inputGroupFile01">Choose Image Slider 1</label>
                               </div>
                               <img class="img-fluid"  src="{{asset($setting->slider_image1)}}"   />
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="custom-file">
                                     <input type="file" name="slider_image2" class="custom-file-input" id="inputGroupFile01">
                                     <label class="custom-file-label" for="inputGroupFile01">Choose Image Slider 2</label>
                               </div>
                               <img class="img-fluid"  src="{{asset($setting->slider_image2)}}"   />
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="custom-file">
                                     <input type="file" name="slider_image3" class="custom-file-input" id="inputGroupFile01">
                                     <label class="custom-file-label" for="inputGroupFile01">Choose Image Slider 3</label>
                               </div>
                               <img class="img-fluid"  src="{{asset($setting->slider_image3)}}"   />
                            </fieldset>
                            <fieldset class="form-group">
                                <div class="custom-file">
                                     <input type="file" name="logo" class="custom-file-input" id="inputGroupFile01">
                                     <label class="custom-file-label" for="inputGroupFile01">Choose Image Slider 3</label>
                               </div>
                               <img class="img-fluid"  src="{{asset($setting->logo)}}"   />
                            </fieldset>


                       <button class="btn btn-primary" type="submit" > Edit  </button>

        </form>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </section>
        <!-- Basic Inputs end -->
        <!-- Inputs Type Options start -->


        <!-- Control Color end -->
       </div>
    </div>
  </div>



  @endsection


