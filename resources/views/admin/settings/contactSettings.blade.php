

@extends('home')





@section('content')


 <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">Contact Us Setting</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>

      </div>
       <div class="content-body">
        <!-- Basic Inputs start -->
        <section class="basic-inputs">
          <div class="row match-height">
            <div class="col-xl-12 col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Add Product</h4>
                </div>
                <div class="card-block">
                  <div class="card-body">

                        <section id="form-repeater">
                            <div class="row">
                              <div class="col-12">
                                <div class="card">
                                  <div class="card-header">
                                    <h4 class="card-title" id="repeat-form">My Information</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                    <div class="heading-elements">
                                      <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                      </ul>
                                    </div>
                                  </div>

                                  <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form action="/admin/EditContactSettings" method="POST">
                                            @csrf

                                            <div class="repeater-default">
                                                <div data-repeater-list="car">
                                                    @foreach ($emails as $email)
                                                  <div data-repeater-item>
                                                    <div   class=" row">
                                                      <div class="form-group mb-1 col-sm-12 col-md-3">
                                                        <label for="email-addr">Email address</label>
                                                        <br>
                                                        <input type="email" name="email" value="{{$email->email}}" class="form-control" id="email-addr" placeholder="Enter email">
                                                      </div>
                                                      <div class="form-group mb-1 col-sm-12 col-md-3">
                                                        <label for="pass">Address</label>
                                                        <br>
                                                        <input type="text" name="address" value="{{$email->address}}" class="form-control" id="pass" placeholder="Address">
                                                      </div>
                                                      <div class="form-group mb-1 col-sm-12 col-md-3">
                                                        <label for="bio" class="cursor-pointer">Phones</label>
                                                        <br>
                                                        <input type="number" name="phone" value="{{$email->phone}}"  class="form-control" id="pass" placeholder="Phones">
                                                    </div>

                                                      <div class="form-group col-sm-12 col-md-2 text-center mt-2">
                                                        <button type="button" class="btn btn-danger" data-repeater-delete> <i class="ft-x"></i> Delete</button>
                                                      </div>

                                                      <button data-repeater-create type="button" class="btn btn-primary">
                                                        <i class="ft-plus"></i> Add
                                                      </button>
                                                    </div>
                                                    <hr>
                                                  </div>
                                                  @endforeach
                                                </div>
                                                <div class="form-group overflow-hidden">
                                                  <div class="col-12">

                                                    <button  type="submit" class="btn btn-primary">
                                                        <i class="ft-plus"></i> Submit
                                                      </button>
                                                  </div>
                                                </div>
                                              </div>


                                    </form>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </section>



                  </div>
                </div>
              </div>
            </div>

          </div>

        </section>
        <!-- Basic Inputs end -->
        <!-- Inputs Type Options start -->


        <!-- Control Color end -->
       </div>
    </div>
  </div>



  @endsection

  @section('externalscripts')
  <script src="{{asset('app-assets/js/scripts/forms/form-repeater.js')}}" type="text/javascript"></script>
  @endsection
