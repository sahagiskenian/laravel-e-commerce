@extends('home')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            @if ($status==0)
            <h3 class="content-header-title">Pending Orders</h3>
            @elseif  ($status==1)
            <h3 class="content-header-title">Accepted Orders</h3>
            @elseif  ($status==2)
            <h3 class="content-header-title">Processing Orders</h3>
            @elseif  ($status==3)
            <h3 class="content-header-title">Delivered Orders</h3>
            @elseif  ($status==4)
            <h3 class="content-header-title">Refused Orders Orders</h3>
            @endif

          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>
        <div class="content-header-right  offset-3 col-md-3 col-12">

            <!-- Modal -->



        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">orders</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                            <th class=" ">Payment_Type</th>
                            <th class=" ">Transction ID</th>

                            <th class=" ">Shipping</th>
                            <th class=" ">Total</th>
                            <th class=" ">Date</th>

                            <th class=" ">Action</th>
                        </tr>
                      </thead>
                    <tbody>
                        @foreach ($orders as $row)
                        <tr>
                            <td>{{ $row->payment_type }}</td>
                            <td>{{ $row->blnc_transection }}</td>

                            <td>{{ $row->shipping }} $</td>
                            <td>{{ $row->total }} $</td>
                            <td>{{ $row->date }}  </td>


                            <td>
                            <a href="/admin/orderDetails/{{$row->id}}" class="btn btn-sm btn-info">View</a>

                            </td>

                          </tr>
                        @endforeach

                    </tbody>


                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
@endsection

