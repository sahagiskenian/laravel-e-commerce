@extends('home')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
<div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">Orderer Info</h4>
          <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
          <div class="heading-elements">
            <ul class="list-inline mb-0">
              <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
              <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
              <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
              <li><a data-action="close"><i class="ft-x"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="card-content collpase show">
          <div class="card-body">


              <div class="form-body">
                <h4 class="form-section"><i class="ft-user"></i> Personal Info</h4>
                <div class="row">
                  <div class="col-md-12">
                     <h4> Name : {{$orderUser->user->name}} </h4>
                  </div>

                </div>
                <div class="row">

                    <div class="col-md-12">
                      <h4> Email : {{$orderUser->user->email}} </h4>
                    </div>
                  </div>
                <div class="row">
                  <div class="col-md-12">
                    <h4> PaymentID : {{$orderUser->payment_id}} </h4>
                  </div>

                </div>
                <div class="row">

                    <div class="col-md-12">
                      <h4> Subtotal : {{$orderUser->paying_amount}} </h4>
                    </div>
                  </div>


                <div class="row">
                  <div class="col-md-12">
                    <h4> Payment Type : {{$orderUser->payment_type}} </h4>
                  </div>

                </div>
                <div class="row">

                    <div class="col-md-12">
                      <h4> Total : {{$orderUser->total}} </h4>
                    </div>
                  </div>

              </div>


          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
              <h4 class="card-title">Shipping Info</h4>
              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                  <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                  <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                  <li><a data-action="close"><i class="ft-x"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="card-content collpase show">
              <div class="card-body">


                  <div class="form-body">
                    <h4 class="form-section"><i class="ft-user"></i> Shipping Info</h4>
                    <div class="row">
                      <div class="col-md-12">
                         <h4> Name : {{$shipping->ship_name}} </h4>
                      </div>

                    </div>
                    <div class="row">

                        <div class="col-md-12">
                          <h4> Email : {{$shipping->ship_email}} </h4>
                        </div>
                      </div>
                    <div class="row">
                      <div class="col-md-12">
                        <h4> City : {{$shipping->ship_city}} </h4>
                      </div>

                    </div>
                    <div class="row">

                        <div class="col-md-12">
                          <h4> Address : {{$shipping->ship_address}} </h4>
                        </div>
                      </div>


                    <div class="row">
                      <div class="col-md-12">
                        <h4> Phone : {{$shipping->ship_phone}} </h4>
                      </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                          <h4> Order Number : {{$shipping->order_id}} </h4>
                        </div>

                      </div>


                  </div>


              </div>
            </div>
          </div>
    </div>
  </div>
    </div>



    <section id="configuration">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Zero configuration</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content collapse show">
                <div class="card-body card-dashboard">

                  <table class="table table-striped table-bordered zero-configuration">
                    <thead>
                      <tr>
                        <th class="wd-15p">Product code</th>
                        <th class="wd-15p">Product Name</th>
                        <th class="wd-15p">Image</th>

                        <th class="wd-15p">Quantity</th>
                        <th class="wd-15p">Unit Price</th>
                        <th class="wd-20p">Total</th>
                      </tr>
                    </thead>
                  <tbody>

                    @foreach($orderProduct as $row)
                    <tr>
                      <td>{{ $row->product->product_code }}</td>
                      <td>{{ $row->product->product_name }}</td>

                 <td> <img src="{{ asset($row->product->main_image) }}" height="50px;" width="50px;"> </td>

                     <td>{{ $row->quantity }}</td>
                     <td>{{ $row->singleprice }}</td>
                     <td>{{ $row->totalprice }}</td>

                    </tr>
                    @endforeach


                  </tbody>


                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<div class="row">
    <div class="col-md-4"> </div>

    <div class="col-md-4">
        @if ($status==0)
        <a  href="/admin/AcceptOrder/{{$orderUser->id}}" class="btn btn-success btn-block">Accept</a>
        <a  href="/admin/RefuseOrder/{{$orderUser->id}}"   class="btn btn-danger btn-block">Refuse</a>

        @elseif  ($status==1)
        <a  href="/admin/ProcessOrder/{{$orderUser->id}}" class="btn btn-success btn-block">Process</a>
        @elseif  ($status==2)
        <a  href="/admin/DeliverOrder/{{$orderUser->id}}" class="btn btn-success btn-block">Delivered</a>
        @elseif  ($status==3)
       <h3 class="alert alert-success"> Order Delivered </h3>
        @elseif  ($status==4)

        @endif

  </div>
</div>

    </div>
  @endsection

