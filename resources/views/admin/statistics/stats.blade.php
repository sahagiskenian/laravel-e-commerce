@extends('home')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">

            <h3 class="content-header-title">Stats</h3>


          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">

            </div>
          </div>
        </div>
        <div class="content-header-right  offset-3 col-md-3 col-12">

            <!-- Modal -->



        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="minimal-statistics-bg">
            <div class="row">
              <div class="col-12 mt-3 mb-1">
                <h4 class="text-uppercase">Shopy's market Stats</h4>
                <p>It's Good For Future Decisions</p>
              </div>
            </div>
            <!-- Crypto Stats 1 -->
            <div id="crypto-stats-1" class="row">
              <div class="col-xl-3 col-lg-6 col-12">
                <div class="card bg-info">
                  <div class="card-content">
                    <div class="card-body pb-1">
                      <div class="row">
                        <div class="col-7">
                          <h4 class="text-white mb-1"><i class="cc XRP" title="XRP"></i> Today Orders</h4>

                        </div>
                        <div class="col-5 text-right">
                          <h4 class="text-white mb-1"> {{$todayOrders}}</h4>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <div class="card bg-warning">
                  <div class="card-content">
                    <div class="card-body pb-1">
                      <div class="row">
                        <div class="col-7">
                          <h4 class="text-white mb-1"><i class="cc BTC" title="BTC"></i> Month Orders</h4>

                        </div>
                        <div class="col-5 text-right">
                          <h4 class="text-white mb-1"> {{$monthOrders}}</h4>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <div class="card bg-danger">
                  <div class="card-content">
                    <div class="card-body pb-1">
                      <div class="row">
                        <div class="col-7">
                          <h4 class="text-white mb-1"><i class="cc ETH" title="ETH"></i> Year Order</h4>

                        </div>
                        <div class="col-5 text-right">
                          <h4 class="text-white mb-1"><i class="la la-caret-up"></i> {{$yearOrders}}</h4>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <div class="card bg-success">
                  <div class="card-content">
                    <div class="card-body pb-1">
                      <div class="row">
                        <div class="col-7">
                          <h4 class="text-white mb-1"><i class="cc LTC" title="LTC"></i> Today Delivered</h4>

                        </div>
                        <div class="col-5 text-right">
                          <h4 class="text-white mb-1"> {{$todayDelivered}}</h4>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Crypto Stats 2 -->
            <div id="crypto-stats-2" class="row">
              <div class="col-xl-3 col-lg-6 col-12">
                <div class="card bg-info">
                  <div class="card-content">
                    <div class="card-body pb-1">
                      <div class="row text-white">
                        <div class="col-6">
                          <i class="cc XRP-alt font-large-1" title="XRP"></i>

                        </div>
                        <div class="col-6 text-right pl-0">
                          <h2 class="text-white mb-2 font-large-1">{{$orderReturned}}</h2>
                          <h4 class="text-white">Returned Orders</h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <div class="card bg-warning">
                  <div class="card-content">
                    <div class="card-body pb-1">
                      <div class="row text-white">
                        <div class="col-6">

                        </div>
                        <div class="col-6 text-right pl-0">
                          <h3 class="text-white mb-2 font-large-1">{{$usersNumber}}</h3>
                          <h4 class="text-white">Users Number</h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <div class="card bg-danger">
                  <div class="card-content">
                    <div class="card-body pb-1">
                      <div class="row text-white">
                        <div class="col-6">
                          <i class="cc ETH-alt font-large-1" title="ETH"></i>

                        </div>
                        <div class="col-6 text-right pl-0">
                          <h3 class="text-white mb-2 font-large-1">{{$categoriesNumber}}</h3>
                          <h4 class="text-white">Categories Number</h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <div class="card bg-success">
                  <div class="card-content">
                    <div class="card-body pb-1">
                      <div class="row text-white">

                        <div class="col-12 text-right pl-0">
                          <h3 class="text-white mb-2 font-large-1">{{$subcategoriesNumber}}</h3>
                          <h4 class="text-white">SubCategories Number </h4>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Crypto Stats 3 -->

          </section>

      </div>
    </div>
  </div>
@endsection

