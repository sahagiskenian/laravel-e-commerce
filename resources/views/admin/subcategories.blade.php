@extends('home')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">Basic DataTables</h3>
          <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a>
                </li>
                <li class="breadcrumb-item"><a href="#">DataTables</a>
                </li>
                <li class="breadcrumb-item active">Basic DataTables
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div class="content-header-right  offset-3 col-md-3 col-12">
            <button type="button" class="btn btn-outline-primary block btn-lg" data-toggle="modal"
            data-target="#default">
             Add Category
            </button>
            <!-- Modal -->
            <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
            aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">SubCategories  </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                 <form action="subcategories" method="POST">
                    @csrf
                    <fieldset class="form-group">
                        <input name="subcategory_name" placeholder="subcategory name" type="text" class="form-control" id="basicInput">

                    </fieldset>

                    <div class="form-group">
                        <select name="category_id" class="custom-select " id="inlineFormCustomSelect">
                            <option selected>Choose Category..</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                            @endforeach


                        </select>
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-primary">Add</button>
                </form>
                  </div>
                </div>
              </div>
            </div>


        @foreach ($subcategories as $subcategory)
        <div class="modal fade text-left" id="s{{$subcategory->id}}" tabindex="-1" role="dialog" aria-labelledby="#s{{$subcategory->id}}"
        aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">subcategory  </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
             <form action="subcategories" method="POST">
                @csrf
                <fieldset class="form-group">
                <input name="subcategory_name" value="{{$subcategory->subcategory_name}}"   type="text" class="form-control" id="basicInput">
                  </fieldset>
                  <div class="form-group">
                    <select name="category_id" class="custom-select " id="inlineFormCustomSelect">

                    @foreach ($categories as $category)
                    @if($category->id == $subcategory->category_id)

                    <option value="{{$category->id}}" selected>{{$category->category_name}}</option>

                    @else
                    <option value="{{$category->id}}"  >{{$category->category_name}}</option>
                    @endif
                    @endforeach


                      </select>
                      </div>
                      <fieldset class="form-group">
                        <input name="id" hidden value="{{$subcategory->id}}"   type="text" class="form-control"  >
                      </fieldset>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-outline-primary">Edit</button>
            </form>
              </div>
            </div>
          </div>
        </div>
        @endforeach
        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Zero configuration</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                            <th>#</th>
                          <th>SubCategory Name</th>
                          <th>Category Name</th>
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                    <tbody>
                        @foreach ($subcategories as $subcategory)
                            <tr>
<td> {{$subcategory->id}} </td>
<td> {{$subcategory->subcategory_name}} </td>
<td> {{$subcategory->category->category_name}} </td>
<td> <button type="button" data-toggle="modal"
data-target="#s{{$subcategory->id}}" class="btn btn-primary"> Edit </button> </td>
<td> <a href="{{route('deletesubcategory',['id'=>$subcategory->id])}}" class="btn btn-danger"> Delete </a> </td>
                            </tr>
                        @endforeach

                    </tbody>


                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
@endsection
@section('externalscripts')
<script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
type="text/javascript"></script>
@endsection
