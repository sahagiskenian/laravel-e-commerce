@extends('home')





@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
          <h3 class="content-header-title">Services</h3>
          <div class="row breadcrumbs-top">

          </div>
        </div>
        <div class="content-header-right  offset-3 col-md-3 col-12">
            <button type="button" class="btn btn-outline-primary block btn-lg" data-toggle="modal"
            data-target="#default">
             Add Service
            </button>
            <!-- Modal -->
            <div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
            aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel1">Service  </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                 <form action="/admin/services" method="POST">
                    @csrf
                    <fieldset class="form-group">
                        <input name="title" placeholder="Service name" type="text" class="form-control" id="basicInput">
                      </fieldset>
                      <fieldset class="form-group">
                        <input name="description" placeholder="Service Description" type="text" class="form-control" id="basicInput">
                      </fieldset>

                      <div class="form-group">
                        <label for="exampleFormControlSelect2">Example multiple select</label>
                        <select   class="form-control" name="logo" id="exampleFormControlSelect2">
                          <option>Select An option</option>
                          <option value="fa fa-tablet">tablet</option>
                          <option value="et-document">document</option>
                          <option value="et-basket">basket</option>

                        </select>
                      </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-outline-primary">Add</button>
                </form>
                  </div>
                </div>
              </div>
            </div>


        @foreach ($services as $service)
        <div class="modal fade text-left" id="s{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="#s{{$service->id}}"
        aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">service  </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
             <form action="/admin/services" method="POST">
                @csrf
                <fieldset class="form-group">
                <input name="title" value="{{$service->title}}"   type="text" class="form-control" id="basicInput">
                  </fieldset>
                  <fieldset class="form-group">
                    <input name="description" value="{{$service->description}}"   type="text" class="form-control" id="basicInput">
                      </fieldset>

                      <div class="form-group">
                        <label for="exampleFormControlSelect2">Example multiple select</label>
                        <select name="logo"  class="form-control" id="exampleFormControlSelect2">
                          <option value="{{$service->logo}}" selected></option>
                          <option value="fa fa-tablet">tablet</option>
                          <option value="et-document">document</option>
                          <option value="et-basket">basket</option>

                        </select>
                      </div>

                  <fieldset class="form-group">
                    <input name="id" hidden value="{{$service->id}}"   type="text" class="form-control"  >
                  </fieldset>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-outline-primary">Edit</button>
            </form>
              </div>
            </div>
          </div>
        </div>
        @endforeach
        </div>
      </div>
      <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Zero configuration</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                            <th>#</th>
                          <th>Name</th>
                          <th>Description</th>
                          <th>Logo</th>
                          <th>Edit</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                    <tbody>
                        @foreach ($services as $service)
                            <tr>
<td> {{$service->id}} </td>
<td> {{$service->title}} </td>
<td> {{$service->description}} </td>
<td> {{$service->logo}} </td>
<td> <button type="button" data-toggle="modal"
data-target="#s{{$service->id}}" class="btn btn-primary"> Edit </button> </td>
<td> <a href="removeservice/{{$service->id}}" class="btn btn-danger"> Delete </a> </td>
                            </tr>
                        @endforeach

                    </tbody>


                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
    </div>
  </div>
@endsection
@section('externalscripts')
<script src="{{asset('app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
type="text/javascript"></script>
@endsection
