
 @php
 $categories= \App\category::with('subcategories')->get();

use App\Setting;
$setting=Setting::first();
$email=json_decode($setting->emails);
$item=$email[0];

 @endphp
<div id="header" class="navbar-toggleable-md sticky clearfix">

    <!-- TOP NAV -->
    <header id="topNav">
        <div class="container">

            <!-- Mobile Menu Button -->
            <button class="btn btn-mobile" data-toggle="collapse" data-target=".nav-main-collapse">
                <i class="fa fa-bars"></i>
            </button>

            <!-- BUTTONS -->
            <ul class="float-right nav nav-pills nav-second-main">

                <!-- SEARCH -->
                @if (Auth::check())

                <li class="search">


                 <form id="logout-form" action="{{ route('logout') }}" method="POST"  >
                     @csrf
       <button type="submit"><i class="fa fa-sign-out"></i>  </button>
                 </form>

             </a>


                </li>
                <li class="search">
                    <a href="/profile">
                        <i class="fa fa-user"></i>
                    </a>

                </li>
                @else
                <li class="search">
                    <a href="/login">
                        <i class="fa fa-sign-in"></i>
                    </a>

                </li>

                @endif

                <!-- /SEARCH -->

            </ul>
            <!-- /BUTTONS -->


            <!-- Logo -->
            <a class="logo float-left" href="/">
                <img src="{{asset($setting->logo)}}" width="150px"   alt="" /> <!-- light logo -->
          </a>

            <!--
                Top Nav

                AVAILABLE CLASSES:
                submenu-dark = dark sub menu
            -->
            <div class="navbar-collapse collapse   nav-main-collapse submenu-dark">
                <nav class="nav-main">

                    <!--
                        NOTE

                        For a regular link, remove "dropdown" class from LI tag and "dropdown-toggle" class from the href.
                        Direct Link Example:

                        <li>
                            <a href="#">HOME</a>
                        </li>
                    -->
                    <ul id="topMain" class="nav nav-pills nav-main">
                        <li class="  active"><!-- HOME -->
                            <a class="" href="/">
                                HOME
                            </a>

                        </li>
                        <li class="dropdown"><!-- HOME -->
                            <a class="dropdown-toggle" href="#">
                                Shop
                            </a>
                            <ul class="dropdown-menu">
                                @foreach ($categories as $category)
                                <li class="dropdown">
                                    <a class="" href="/categoryProducts/{{$category->id}}">
                                        {{$category->category_name}}
                                    </a>

                                </li>

                                @endforeach


                            </ul>
                        </li>

                        <li ><!-- PAGES -->
                            <a   href="/aboutus">
                                AboutUs
                            </a>

                        </li>
                        <li ><!-- PAGES -->
                            <a   href="/contactus">
                                ContactUs
                            </a>

                        </li>

@if (Auth::user())
  <li ><!-- PAGES -->
    <a   href="/Wishlist">
       <i class="fa fa-heart" > </i>
    </a>

</li>
<li ><!-- PAGES -->
    <a   href="/Cart">
       <i class="fa fa-shopping-cart" > </i>
    </a>

</li>

@endif



                        <!--
                            MENU ANIMATIONS
                                .nav-animate-fadeIn
                                .nav-animate-fadeInUp
                                .nav-animate-bounceIn
                                .nav-animate-bounceInUp
                                .nav-animate-flipInX
                                .nav-animate-flipInY
                                .nav-animate-zoomIn
                                .nav-animate-slideInUp

                                .nav-hover-animate 		= animate text on hover

                                .hover-animate-bounceIn = bounceIn effect on mouse over of main menu
                        -->

                    </ul>

                </nav>
            </div>

        </div>
    </header>
    <!-- /Top Nav -->

</div>
