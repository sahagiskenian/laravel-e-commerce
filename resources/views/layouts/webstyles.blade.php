<link href="{{asset('webassets/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('webassets/assets/plugins/slider.revolution/css/extralayers.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('webassets/assets/plugins/slider.revolution/css/settings.css')}}" rel="stylesheet" type="text/css" />

<!-- THEME CSS -->
<link href="{{asset('webassets/assets/css/essentials.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('webassets/assets/css/layout.css')}}" rel="stylesheet" type="text/css" />

<!-- PAGE LEVEL SCRIPTS -->
<link href="{{asset('webassets/assets/css/header-1.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('webassets/assets/css/color_scheme/green.css')}}" rel="stylesheet" type="text/css" id="color_scheme" />

<!-- REVOLUTION SLIDER -->
<link href="{{asset('webassets/assets/plugins/slider.revolution/css/extralayers.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('webassets/assets/plugins/slider.revolution/css/settings.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('webassets/assets/css/header-1.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('webassets/assets/css/layout-shop.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('webassets/assets/css/color_scheme/green.css')}}" rel="stylesheet" type="text/css" id="color_scheme" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
@livewireStyles
