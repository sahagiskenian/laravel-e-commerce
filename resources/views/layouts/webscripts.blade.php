<script>var plugin_path = '{{asset('webassets/assets/plugins/')}}';</script>
<script src="{{asset('/webassets/assets/plugins/jquery/jquery-3.3.1.min.js')}}"></script>



<script src="{{asset('/webassets/assets/js/scripts.js')}}"></script>




<!-- STYLESWITCHER - REMOVE -->

<!-- REVOLUTION SLIDER -->
<script src="{{asset('/webassets/assets/plugins/slider.revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('/webassets/assets/plugins/slider.revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{asset('/webassets/assets/js/view/demo.revolution_slider.js')}}"></script>


<script src="{{asset('/assets/js/sub.js')}}"></script>
<!-- PAGE LEVEL SCRIPTS -->
<script src="{{asset('/webassets/assets/js/view/demo.shop.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
<script>
    @if(Session::has('messege'))
      var type="{{Session::get('alert-type','info')}}"
      switch(type){
          case 'info':
               toastr.info("{{ Session::get('messege') }}");
               break;
          case 'success':
              toastr.success("{{ Session::get('messege') }}");
              break;
          case 'warning':
             toastr.warning("{{ Session::get('messege') }}");
              break;
          case 'error':
              toastr.error("{{ Session::get('messege') }}");
              break;
      }
    @endif
 </script>
 <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5fd9ea507c55982e"></script>

@livewireScripts
