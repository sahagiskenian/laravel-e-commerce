@php
use App\Setting;
$setting=Setting::first();
$email=json_decode($setting->emails);
$item=$email[0];
@endphp


<footer id="footer">
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <!-- Footer Logo -->
                <img class="footer-logo" src="{{asset($setting->logo)}}" width="100px" alt="" />

                <!-- Small Description -->
                <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>

                <!-- Contact Address -->
                <address>

                    <ul class="list-unstyled">
                        <li class="footer-sprite address">
                            {{$item->address}}
                        </li>
                        <li class="footer-sprite phone">
                            {{$item->phone}}
                        </li>
                        <li class="footer-sprite email">
                            <a href="mailto:{{$item->email}}">   {{$item->email}}</a>
                        </li>
                    </ul>


                </address>
                <!-- /Contact Address -->

            </div>



            <div class="col-md-4">

                <!-- Links -->
                <h4 class="letter-spacing-1">EXPLORE SHOP</h4>
                <ul class="footer-links list-unstyled">
                    <li><a href="/main">Home</a></li>
                    <li><a href="/aboutus">About Us</a></li>
                    <li><a href="#">Our Shop</a></li>

                    <li><a href="/contactus">Contact Us</a></li>
                </ul>
                <!-- /Links -->

            </div>

            <div class="col-md-4">

                <!-- Newsletter Form -->
                <h4 class="letter-spacing-1">KEEP IN TOUCH</h4>
                <p>Subscribe to Our Newsletter to get Important News &amp; Offers</p>

                <form class="validate" action="/Subscribe" id="subscribe" method="post" data-success="Subscribed! Thank you!" data-toastr-position="bottom-right">
                    @csrf
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="email" id="email" name="email" class="form-control required" placeholder="Enter your Email">
                        <span class="input-group-btn">
                            <button class="btn btn-success" type="submit">Subscribe</button>
                        </span>
                    </div>
                </form>
                <!-- /Newsletter Form -->

                <!-- Social Icons -->
                <div class="mt-20">
                    <a href="#" class="social-icon social-icon-border social-facebook float-left" data-toggle="tooltip" data-placement="top" title="Facebook">

                        <i class="icon-facebook"></i>
                        <i class="icon-facebook"></i>
                    </a>

                    <a href="#" class="social-icon social-icon-border social-twitter float-left" data-toggle="tooltip" data-placement="top" title="Twitter">
                        <i class="icon-twitter"></i>
                        <i class="icon-twitter"></i>
                    </a>

                    <a href="#" class="social-icon social-icon-border social-gplus float-left" data-toggle="tooltip" data-placement="top" title="Google plus">
                        <i class="icon-gplus"></i>
                        <i class="icon-gplus"></i>
                    </a>

                    <a href="#" class="social-icon social-icon-border social-linkedin float-left" data-toggle="tooltip" data-placement="top" title="Linkedin">
                        <i class="icon-linkedin"></i>
                        <i class="icon-linkedin"></i>
                    </a>

                    <a href="#" class="social-icon social-icon-border social-rss float-left" data-toggle="tooltip" data-placement="top" title="Rss">
                        <i class="icon-rss"></i>
                        <i class="icon-rss"></i>
                    </a>

                </div>
                <!-- /Social Icons -->

            </div>

        </div>

    </div>

    <div class="copyright">
        <div class="container">
            <ul class="float-right m-0 list-inline mobile-block">

                <li><a href="/contactus">Contact Us</a></li>
            </ul>
            &copy; All Rights Reserved, <a  href="https://floating-dusk-38928.herokuapp.com/"
            target="_blank">Sahag  </a>
        </div>
    </div>
</footer>
