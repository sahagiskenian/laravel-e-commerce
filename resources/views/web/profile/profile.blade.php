@extends('welcome')
@section('title')
<title>{{Auth::user()->name}}| Shopy's Market</title>
<meta name="description" content="Market section">
<meta name="keywords" content="shop tags, shop description, shop keywords, SEO, search engine optimization">
@endsection
@php
$n=0;
foreach (Auth::user()->orders as  $value) {
   if($value->status==3)
   $n++;
}

@endphp

@section('content')

<section class="page-header page-header-xs">
    <div class="container">

        <!-- breadcrumbs -->
        <ol class="breadcrumb breadcrumb-inverse">
            <li><a href="#">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Felicia Doe</li>
        </ol><!-- /breadcrumbs -->

    </div>
</section>
<!-- /PAGE HEADER -->




<!-- -->
<section>
    <div class="container">
        <div class="row">

            <!-- LEFT -->
            <div class="col-lg-3 col-md-3 col-sm-4">
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                      Check Order
                </button>

                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Check Order</h4>
                            </div>
                          <form action="/checkOrder" method="POST">
                            <!-- Modal Body -->
                            @csrf
                            <div class="modal-body">
                                <input required class="form-control word-count" name="status_code" type="text" data-maxlength="100" rows="5" placeholder="Type your status code..."/>

                            </div>

                            <!-- Modal Footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Check</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
                <div class="thumbnail text-center">
                    <img class="img-fluid" src="demo_files/images/people/460x700/8-min.jpg" alt="" />
                    <h2 class="fs-18 mt-10 mb-0">{{Auth::user()->name}}</h2>

                </div>

                <!-- completed -->

                <!-- /completed -->

                <!-- SIDE NAV -->

                <!-- /SIDE NAV -->


                <!-- info -->
                <div class="box-light mb-30"><!-- .box-light OR .box-light -->
                    <div class="row mb-20">
                        <div class="col-md-6 col-sm-6 col-6 text-center bold">
                            <h2 class="fs-30 mt-10 mb-0 font-raleway">{{count(Auth::user()->orders)}}</h2>
                            <h3 class="fs-11 mt-0 mb-10 text-info">Orders</h3>
                        </div>

                        <div class="col-md-6 col-sm-6 col-6 text-center bold">
                            <h2 class="fs-30 mt-10 mb-0 font-raleway">{{$n}}</h2>
                            <h3 class="fs-11 mt-0 mb-10 text-info">Delivered Orders</h3>
                        </div>


                    </div>
                    <!-- /info -->

                    <div class="text-muted">
                        <h2 class="fs-18 text-muted mb-6"><b>About</b> Felicia Doe</h2>
                        <p>Lorem ipsum dolor sit amet diam nonummy nibh dolore.</p>

                        <ul class="list-unstyled m-0">
                            <li class="mb-10"><i class="fa fa-globe fw-20 hidden-xs-down hidden-sm"></i> <a href="http://www.stepofweb.com">www.stepofweb.com</a></li>
                            <li class="mb-10"><i class="fa fa-facebook fw-20 hidden-xs-down hidden-sm"></i> <a href="http://www.facebook.com/stepofweb">stepofweb</a></li>
                            <li class="mb-10"><i class="fa fa-twitter fw-20 hidden-xs-down hidden-sm"></i> <a href="http://www.twitter.com/stepofweb">@stepofweb</a></li>
                        </ul>
                    </div>

                </div>

            </div>


            <!-- RIGHT -->
            <div class="col-lg-9 col-md-9 col-sm-8">

                <!-- FLIP BOX -->
                <div class="box-flip box-icon box-icon-center box-icon-round box-icon-large text-center m-0">
                    <div class="front">
                        <div class="box1 rad-0">
                            <div class="box-icon-title">
                                <i class="fa fa-user"></i>
                                <h2>{{Auth::user()->name}}</h2>
                            </div>
                            <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere</p>
                        </div>
                    </div>

                    <div class="back">
                        <div class="box2 rad-0">
                            <h4>WHO AM I?</h4>
                            <hr />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque. Ut enim massa, sodales tempor convallis et, iaculis ac massa. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque.</p>
                        </div>
                    </div>
                </div>
                <!-- /FLIP BOX -->


                <div class="box-light"><!-- .box-light OR .box-dark -->




                    <div class="row mt-30">

                        <!-- DISCUSSIONS -->
                        <div class="col-md-12 col-sm-12">

                            <div class="box-inner">
                                <h3>
                                    <a class="float-right fs-11 text-warning" href="#">VIEW ALL</a>
                                    Orders
                                </h3>
                                <div class="h-250 slimscroll" data-always-visible="true" data-size="5px" data-position="right" data-opacity="0.4" disable-body-scroll="true">
@foreach (Auth::user()->orders as $item)
<div class="clearfix mb-20"><!-- discussion item -->

    <h4 class="fs-15 m-0 b-0 p-0 bold"><a href="#">Order Code : {{$item->status_code}}</a></h4>
    <span class="fs-13 text-muted">
      paying amount: {{$item->paying_amount}} -- All Total : {{$item->total}} -- Date : {{$item->date}}
      @if ($item->status==3)
      <i class="fa fa-check" > </i>

      @endif
      Return Status: @if ($item->return_order==0)
          <a href="/returnOrder/{{$item->id}}" class="btn btn-danger"  >Return  </a>
          @elseif($item->return_order==1)
          <span class="badge badge-primary"> pending </span>
          @else
          <span class="badge badge-success">  Returned   </span>
      @endif
    </span>
</div><!-- /discussion item -->

@endforeach


                                </div>
                            </div>

                            <div class="box-footer">
                                <!-- INLINE SEARCH -->
                                <div class="inline-search clearfix">
                                    <form action="#" method="get" class="widget_search m-0">
                                        <input type="search" placeholder="Search Discussion..." name="s" class="serch-input">
                                        <button type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <div class="clear"></div>
                                    </form>
                                </div>
                                <!-- /INLINE SEARCH -->

                            </div>

                        </div>
                        <!-- /DISCUSSIONS -->


                    </div>


                </div>


                <form method="post" action="/changePassword" class="box-light mt-20">
                    @csrf
                    <div class="box-inner">
                        <h4 class="uppercase">Change Password</strong></h4>

                        <input required class="form-control word-count" name="password" type="password" data-maxlength="100" rows="5" placeholder="Type your password here..."/>


                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Change Password</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</section>
@endsection
