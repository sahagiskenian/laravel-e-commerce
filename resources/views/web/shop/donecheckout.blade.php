@extends('welcome')
@section('title')
<title>Check out Done Section | Shopy's Market</title>
<meta name="description" content="Market section">
<meta name="keywords" content="shop tags, shop description, shop keywords, SEO, search engine optimization">
@endsection


@section('content')
<section class="page-header">
    <div class="container">

        <h1>ORDER PLACED</h1>

        <!-- breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Shop</a></li>
            <li class="active">Checkout Final</li>
        </ol><!-- /breadcrumbs -->

    </div>
</section>
<!-- /PAGE HEADER -->




<!-- -->
<section>
    <div class="container">

        <!-- CHECKOUT FINAL MESSAGE -->
        <div class="card card-default">
            <div class="card-block">
                <h3>Thank you, {{Auth::user()->name}}.</h3>

                <p>
                    Your order has been placed. In a few moments you will receive an order confirmation email from us.<br />
                    If you like, you can explore more <a href="/">Shopy's Products</a>.
                </p>

                <hr />

                <p>
                    Thank you very much for choosing us,<br />
                    <strong>Sahag Inc.</strong>
                </p>
            </div>
        </div>
        <!-- /CHECKOUT FINAL MESSAGE -->

    </div>
</section>
@endsection
