@extends('welcome')
@section('title')
<title>Payment Section | Shopy's Market</title>
<meta name="description" content="Market section">
<meta name="keywords" content="shop tags, shop description, shop keywords, SEO, search engine optimization">
@endsection


@section('content')
<section class="page-header page-header-xs">
    <div class="container">

        <h1>Final CheckOut</h1>

        <!-- breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Shop</a></li>
            <li class="active">Checkout</li>
        </ol><!-- /breadcrumbs -->

    </div>
</section>
<!-- /PAGE HEADER -->




<!-- CART -->
<section>
    <div class="container">


        <!-- NOT LOGGED IN -->

        <!-- /NOT LOGGED IN -->



        <!-- CHECKOUT -->
       <div class="row">
        <div class="col-lg-5 col-sm-5">
            <form action="/payment" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="ship_name" value="{{ $data['name'] }} ">
                <input type="hidden" name="ship_phone" value="{{ $data['phone'] }} ">
                <input type="hidden" name="ship_email" value="{{ $data['email'] }} ">
                <input type="hidden" name="ship_address" value="{{ $data['address'] }} ">
                <input type="hidden" name="ship_city" value="{{ $data['city'] }} ">
                <input type="hidden" name="total" value="{{ $sum *100 }} ">
                <script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="pk_test_51HwSvoAAirG5o3BvMnCCntBsmAHy71Fb43AAILIeCRujVNAqLufp2M5MP2wQyFdoUIz8iooGppvsZxUe8DB0vv4U00GE1t5HuZ"
                data-amount="{{ ($sum+8)*100 }}"
                data-name="Udemy E-commerce tutorial"
                data-description="Buy some books"
                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                data-locale="auto">
                </script>
            </form>



            <div class="toggle-transparent toggle-bordered-full clearfix">
                <div class="toggle active">
                    <div class="toggle-content">

                        <span class="clearfix">
                            <span class="float-right">${{$sum}}</span>
                            <strong class="float-left">Subtotal:</strong>
                        </span>

                        <span class="clearfix">
                            <span class="float-right">$8.00</span>
                            <span class="float-left">Shipping:</span>
                        </span>

                        <hr />

                        <span class="clearfix">
                            <span class="float-right fs-20">${{$sum +8}}</span>
                            <strong class="float-left">TOTAL:</strong>
                        </span>

                        <hr />

                  </div>
                </div>
            </div>

            <button class="btn btn-primary btn-lg btn-block fs-15"><i class="fa fa-mail-forward"></i> Place Order Now</button>


              </div>
              <div class="col-md-6">


            </div>

            </div> </div>
</section>
@endsection
