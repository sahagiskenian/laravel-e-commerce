@extends('welcome')
@section('title')
<title>Payer Informations | Shopy's Market</title>
<meta name="description" content="Market section">
<meta name="keywords" content="shop tags, shop description, shop keywords, SEO, search engine optimization">
@endsection


@section('content')
<section class="page-header page-header-xs">
    <div class="container">

        <h1>CHECKOUT</h1>

        <!-- breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Shop</a></li>
            <li class="active">Checkout</li>
        </ol><!-- /breadcrumbs -->

    </div>
</section>
<!-- /PAGE HEADER -->




<!-- CART -->
<section>
    <div class="container">


        <!-- NOT LOGGED IN -->

        <!-- /NOT LOGGED IN -->



        <!-- CHECKOUT -->
        <form class="row clearfix" method="post" action="/Finalcheckout">
           @csrf
            <div class="col-lg-12 col-sm-12">
                <div class="heading-title">
                    <h4>Billing &amp; Shipping</h4>
                </div>


                <!-- BILLING -->
                <fieldset class="mt-60">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <label for="billing_firstname">Full Name *</label>
                            <input id="billing_firstname" name="ship_name" type="text" class="form-control required" />
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <label for="billing_email">Email *</label>
                            <input id="billing_email" name="ship_email" type="text" class="form-control required" />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <label for="billing_company">Company</label>
                            <input id="billing_company" name="ship_city" type="text" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <label for="billing_address1">Address *</label>
                            <input id="billing_address1" name="ship_address" type="text" class="form-control required" placeholder="Address " />

                         </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <label for="billing_city">Phone *</label>
                            <input id="billing_city" name="ship_phone" type="number" class="form-control required" />
                        </div>

                    </div>





                    <hr />



                </fieldset>
                <!-- /BILLING -->


                <!-- SHIPPING -->

                <!-- /SHIPPING -->

            </div>



                  <button class="btn btn-primary btn-lg btn-block fs-15"><i class="fa fa-mail-forward"></i> Place Order Now</button>



        </form>
    </div>
</section>
@endsection
