@extends('welcome')
@section('title')
<title>{{$product->product_name}} | Shopy's Market</title>
<meta name="description" content="Market {{$product->product_name}} descrips ">
<meta name="keywords" content="shop tags, shop description, shop keywords, SEO, search engine optimization">
@endsection


@section('content')
<section class="page-header page-header-xs">
    <div class="container">

        <h1>{{$product->product_name}}</h1>


    </div>
</section>
<!-- /PAGE HEADER -->




<!-- -->
<section>
    <div class="container">

        <div class="row">

            <!-- IMAGE -->
            <div class="col-lg-4 col-sm-4">

                <div class="thumbnail relative mb-3">

                    <!--
                        IMAGE ZOOM

                        data-mode="mouseover|grab|click|toggle"
                    -->
                    <figure id="zoom-primary" class="zoom" data-mode="mouseover">
                        <!--
                            zoom buttton

                            positions available:
                                .bottom-right
                                .bottom-left
                                .top-right
                                .top-left
                        -->
                        <a class="lightbox bottom-right" href="{{asset($product->main_image)}}" data-plugin-options='{"type":"image"}'><i class="fa fa-search"></i></a>

                        <!--
                            image

                            Extra: add .image-bw class to force black and white!
                        -->
                        <img class="img-fluid" src="{{asset($product->main_image)}}" width="1200" height="1500" alt="This is the product title" />
                    </figure>

                </div>

                <!-- Thumbnails (required height:100px) -->
                <div data-for="zoom-primary" class="zoom-more owl-carousel owl-padding-3 featured" data-plugin-options='{"singleItem": false, "autoPlay": false, "navigation": true, "pagination": false}'>
                   @foreach ( $product->productImages as $image )
                   <a class="thumbnail active" href="{{asset($image->product_image)}}">
                    <img src="{{asset($image->product_image)}}" height="100" alt="" />
                </a>
                   @endforeach



                </div>
                <!-- /Thumbnails -->

            </div>
            <!-- /IMAGE -->

            <!-- ITEM DESC -->
            <div class="col-lg-5 col-sm-8">

                <!-- buttons -->
                <div class="float-right">
                    <!-- replace data-item-id width the real item ID - used by js/view/demo.shop.js -->
                    @if (Auth::check())
                    <form  action="" method="POST" id="wishlist">
                        @csrf
                    <input type="text"  name="user_id" value="{{Auth::user()->id}}"  hidden/>
                        <input type="text" name="product_id" value="{{$product->id}}"     hidden/>
                     <button class="btn btn-light add-wishlist" type="submit"  data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart p-0"></i></button>
                    </form>
                    @endif

                </div>
                <!-- /buttons -->

                <!-- price -->
                <div class="shop-item-price">
                    @if ($product->discount)
                    <span class="line-through pl-0">{{$product->price }}</span>
                   {{ $product->price -  ($product->price * $product->discount)/100}}
                   @else
                   {{$product->price}}
                   @endif

                </div>
                <!-- /price -->

                <!-- Go to www.addthis.com/dashboard to customize your tools -->

                <hr />

                <div class="clearfix mb-30">
                    <span class="float-right text-success"><i class="fa fa-check"></i> In Stock</span>
                    <!--
                    <span class="float-right text-danger"><i class="fa fa-remove"></i> Out of Stock</span>
                    -->

                    <strong>SKU:</strong> {{$product->product_code}}
                </div>


                <!-- short description -->
                <p>{!! nl2br($product->product_description) !!}</p>
                <!-- /short description -->


                <!-- countdown -->

                <!-- /countdown -->

                <div class="addthis_inline_share_toolbox"></div>

                <hr />

                <!-- FORM -->
                @if (Auth::check())
                <form class="clearfix form-inline m-0" id="cartAdd" method="POST">

                    <!-- see assets/js/view/demo.shop.js -->



                        @csrf
                    <input type="text"  name="user_id" value="{{Auth::user()->id}}"  hidden/>
                        <input type="text" name="product_id" value="{{$product->id}}"     hidden/>
                        <button type="submit" class="btn btn-primary float-left  " ><i class="fa fa-cart-plus"></i> Add to Cart</button>



                </form>
                @endif

                <!-- /FORM -->


                <hr />

                <!-- Share -->

                <!-- /Share -->


                <!-- rating -->

                <div class="rating rating-{{$Rate}} fs-13 mt-10 fw-100"><!-- rating-0 ... rating-5 --></div>
                <!-- /rating -->

            </div>
            <!-- /ITEM DESC -->

            <!-- INFO -->
            <div class="col-sm-4 col-md-3">

                <h4 class="fs-18">
                    <i class="fa fa-paper-plane-o"></i>
                    FREE SHIPPING
                </h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla.</p>

                <h4 class="fs-18">
                    <i class="fa fa-clock-o"></i>
                    30 DAYS MONEY BACK
                </h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla.</p>

                <h4 class="fs-18">
                    <i class="fa fa-users"></i>
                    CUSTOMER SUPPORT
                </h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla.</p>

                <hr>

                <p class="fs-11">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas metus nulla, commodo a sodales sed, dignissim pretium nunc. Nam et lacus neque.
                </p>
            </div>
            <!-- /INFO -->

        </div>



        <ul id="myTab" class="nav nav-tabs nav-top-border mt-80" role="tablist">
            <li class="nav-item"><a class="nav-link active" href="#description" data-toggle="tab">Description</a></li>
            <li class="nav-item"><a class="nav-link" href="#specs" data-toggle="tab">Specifications</a></li>
            <li class="nav-item"><a class="nav-link" href="#reviews" data-toggle="tab">Reviews (2)</a></li>
        </ul>


        <div class="tab-content pt-20">

            <!-- DESCRIPTION -->
            <div role="tabpanel" class="tab-pane active" id="description">
                <p>{!! nl2br($product->product_description) !!}
                   </p>
            </div>

            <!-- SPECIFICATIONS -->
            <div role="tabpanel" class="tab-pane fade" id="specs">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Column name</th>
                                <th>Column name</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($product->sizes)
                            <tr>
                                <td>Size</td>
                                <td>
                                    @foreach ($product->sizes as $size)
                                        {{$size->size_name}}
                                    @endforeach
                                </td>
                            </tr>
                            @endif
                            <tr>
                                <td>Color</td>
                                <td>Red</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>

            <!-- REVIEWS -->
            <div role="tabpanel" class="tab-pane fade" id="reviews">
                <div id="commentsList">
                <!-- REVIEW ITEM -->
                @foreach ($product->comments as $comment)
                <div class="block mb-60">

                    <span class="user-avatar"><!-- user-avatar -->
                        <img class="float-left media-object" src="{{asset('webassets/assets/images/_smarty/avatar2.jpg')}}" width="64" height="64" alt="">
                    </span>

                    <div class="media-body">
                        <h4 class="media-heading fs-14">
                            {{$comment->comment_name}} &ndash;
                            <span class="text-muted">{{$comment->created_at}}</span>
                        -    <div class="rating rating-{{$comment->product_rating}} fs-13 mt-10 fw-100"><!-- rating-0 ... rating-5 --></div>
                        </h4>

                        <p>
                       {{$comment->comment_description}}
                        </p>

                    </div>

                </div>
                @endforeach

                <!-- /REVIEW ITEM -->

                <!-- REVIEW ITEM -->

                <!-- /REVIEW ITEM -->

                </div>
                <!-- REVIEW FORM -->
                <h4 class="page-header mb-40">ADD A REVIEW</h4>
                <form method="post" action="#" id="review">

                    <div class="row mb-10">

                        <div class="col-md-6 mb-10">
                            <!-- Name -->
                            <input type="text" name="comment_name" id="comment_name" class="form-control" placeholder="Name *" maxlength="100" required="">
                        </div>

                        <div class="col-md-6">
                            <!-- Email -->
                            <input type="email" name="comment_email" id="comment_email" class="form-control" placeholder="Email *" maxlength="100" required="">
                        </div>
                        <div  >
                            <!-- Email -->
                        <input type="text" name="product_id" id="product_id" value="{{$product->id}}" class="form-control" hidden >
                        </div>
                    </div>

                    <!-- Comment -->
                    <div class="mb-30">
                        <textarea name="comment_description" id="comment_description" class="form-control" rows="6" placeholder="Comment" maxlength="1000"></textarea>
                    </div>


                    <!-- Stars -->
                    <div class="product-star-vote clearfix">

                        <label class="radio float-left">
                            <input type="radio" name="product_rating" value="1" />
                            <i></i> 1 Star
                        </label>

                        <label class="radio float-left">
                            <input type="radio" name="product_rating" value="2" />
                            <i></i> 2 Stars
                        </label>

                        <label class="radio float-left">
                            <input type="radio" name="product_rating" value="3" />
                            <i></i> 3 Stars
                        </label>

                        <label class="radio float-left">
                            <input type="radio" name="product_rating" value="4" />
                            <i></i> 4 Stars
                        </label>

                        <label class="radio float-left">
                            <input type="radio" name="product_rating" value="5" />
                            <i></i> 5 Stars
                        </label>

                    </div>

                    <!-- Send Button -->
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Send Review</button>

                </form>
                <!-- /REVIEW FORM -->

            </div>
        </div>


        <hr class="mt-80 mb-80" />


        <!-- RELATED -->
        <h2 class="owl-featured"><strong>Related</strong> products:</h2>
        <div class="owl-carousel featured m-0 owl-padding-10" data-plugin-options='{"singleItem": false, "items": "5", "stopOnHover":false, "autoPlay":4500, "autoHeight": false, "navigation": true, "pagination": false}'>
@foreach ($relatedproducts as $product)
      <!-- item -->
      <div class="shop-item">

        <div class="thumbnail">
            <!-- product image(s) -->
            <a class="shop-item-image" href="/productDetails/{{$product->id}}">
                <img   src="{{asset($product->main_image)}}" class="img-fluid" alt="shop first image" />
           </a>
            <!-- /product image(s) -->

            <!-- hover buttons -->
            <div class="shop-option-over"><!-- replace data-item-id width the real item ID - used by js/view/demo.shop.js -->

               @if (Auth::check())


               <form  class="wishlistAdd" method="POST">
                @csrf
            <input type="text"  name="user_id" value="{{Auth::user()->id}}"  hidden/>
                <input type="text" name="product_id" value="{{$product->id}}"     hidden/>
             <button class="btn btn-light add-wishlist" type="submit"  data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart p-0"></i></button>
            </form>





               @endif


            </div>
            <!-- /hover buttons -->

            <!-- product more info -->
            <div class="shop-item-info">
                <span class="badge badge-success">NEW</span>
                @if ($product->discount > 0 )
                <span class="badge badge-danger">SALE</span>
                @endif

            </div>
            <!-- /product more info -->
        </div>

        <div class="shop-item-summary text-center">
            <h2>{{$product->product_name}}</h2>

            <!-- rating -->
            <div class="shop-item-rating-line">
                <div class="rating rating-4 fs-13"><!-- rating-0 ... rating-5 --></div>
            </div>
            <!-- /rating -->


            @if ($product->discount)
            <div class="shop-item-price">
                <span class="line-through">{{$product->price}}</span>
                {{$product->price - ($product->price*$product->discount/100)}}
            </div>
            @else
            <div class="shop-item-price">
                {{$product->price}}

            </div>
            @endif


        </div>

            <!-- buttons -->

            <div class="shop-item-buttons text-center">
                @if (Auth::check())
                <form  class="cartAdd" method="POST">
                    @csrf
                <input type="text"  name="user_id" value="{{Auth::user()->id}}"  hidden/>
                    <input type="text" name="product_id" value="{{$product->id}}"     hidden/>
                    <button type="submit" class="btn btn-light" ><i class="fa fa-cart-plus"></i> Add to Cart</button>
                </form>
                @endif
           </div>
            <!-- /buttons -->
    </div>
    <!-- /item -->
@endforeach




        </div>

    </div>
</section>
@endsection

@section('externalscripts')
<script>

    $( document ).ready(function() {
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#wishlist").submit(function(event){
        event.preventDefault();
        var user_id = $("input[name=user_id]").val();
        var product_id = $("input[name=product_id]").val();


        $.ajax({
          url: "/AddToWishList",
          type:"POST",
          data:{
            user_id:user_id,
            product_id:product_id,

          },
          success:function(response){
            toastr.success('Added To WishList Succesfully')

            },
            error: function(xhr, status, error) {
                console.log(xhr);
                toastr.warning('This One Exists in Your WishList')
}



         });
    });
    $("#review").submit(function(event){
        event.preventDefault();


        $.ajax({
          url: "/AddComment",
          type:"POST",
          data: $("#review").serialize(),
          success:function(response){
            toastr.success('Thank You For Your Review')
            var comment=`
            <div class="block mb-60">

<span class="user-avatar"><!-- user-avatar -->
    <img class="float-left media-object" src="{{asset('webassets/assets/images/_smarty/avatar2.jpg')}}" width="64" height="64" alt="">
</span>

<div class="media-body">
    <h4 class="media-heading fs-14">
        ${response.comment_name} &ndash;
        <span class="text-muted">${response.created_at}</span>

    </h4>

    <p>
        ${response.comment_description}
    </p>

</div>

</div>

            `
            $("#commentsList").append(comment);

            },
            error: function(xhr, status, error) {
                console.log(xhr);
                toastr.warning('This One Exists in Your WishList')
}



         });
    });

    $("#cartAdd").submit(function(event){
        event.preventDefault();


        $.ajax({
          url: "/AddToCart",
          type:"POST",
          data:$("#cartAdd").serialize(),
          success:function(response){
            toastr.success('Added To Cart Succesfully')

            },
            error: function(xhr, status, error) {
                console.log(xhr);
                toastr.warning('This One Exists in Your Cart')
}

         });
    });


    $(".cartAdd").submit(function(event){
        event.preventDefault();
        var form = $(this);
        var data = form.serialize();

        $.ajax({
          url: "/AddToCart",
          type:"POST",
          data,
          success:function(response){
            toastr.success('Added To Cart Succesfully')

            },
            error: function(xhr, status, error) {
                console.log(xhr);
                toastr.warning('This One Exists in Your Cart')
}

         });
    });
    $(".wishlistAdd").submit(function(event){
        event.preventDefault();
        var user_id = $("input[name=user_id]").val();
        var product_id = $("input[name=product_id]").val();
        var form = $(this);
        var data = form.serialize();

        $.ajax({
          url: "/AddToWishList",
          type:"POST",
          data,
          success:function(response){
            toastr.success('Added To WishList Succesfully')

            },
            error: function(xhr, status, error) {
                console.log(xhr);
                toastr.warning('This One Exists in Your WishList')
}



         });
    });

});

  </script>



@endsection
