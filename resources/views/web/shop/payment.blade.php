@extends('welcome')
@section('title')
<title>Payment Section | Shopy's Market</title>
<meta name="description" content="Market section">
<meta name="keywords" content="shop tags, shop description, shop keywords, SEO, search engine optimization">
@endsection



@section('content')
<div class="container">

    <div class="row">
<form action="/payment" method="POST">
    {{ csrf_field() }}
    <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_51HwSvoAAirG5o3BvMnCCntBsmAHy71Fb43AAILIeCRujVNAqLufp2M5MP2wQyFdoUIz8iooGppvsZxUe8DB0vv4U00GE1t5HuZ"
    data-amount="{{  100 }}"
    data-name="Udemy E-commerce tutorial"
    data-description="Buy some books"
    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
    data-locale="auto">
    </script>
</form>
</div>
</div>
@endsection
