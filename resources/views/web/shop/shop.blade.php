@extends('welcome')
@section('title')
<title>Shop | Shopy's Market</title>
<meta name="description" content="Market Shop">
<meta name="keywords" content="shop tags, shop description, shop keywords, SEO, search engine optimization">
@endsection


@section('content')
<section class="page-header page-header-xs">
    <div class="container">

        <h1>SHOP</h1>

        <!-- breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Shop</a></li>
            <li class="active">4 Columns</li>
        </ol><!-- /breadcrumbs -->

    </div>
</section>
<!-- /PAGE HEADER -->




<!-- -->
<section>

    <div>
        <div class="container">
            @if (count($products)==0)
                No Products with this filters

                @else
                <div class="clearfix shop-list-options mb-20">

                    <form  action="/filterProducts" method="GET">
   @csrf
                   <div class="options-left">
                       <select name="sizeId">
                           <option value="all">Choose Size</option>
                       @foreach ($sizes as $size)
                       <option value="{{$size->id}}">{{$size->size_name}}</option>
                       @endforeach
                   </select>

                   <select name="subcategoryId">
                       <option value="all">Choose Kind</option>
                   @foreach ($subcategories as $subcategory)
                   <option value="{{$subcategory->id}}">{{$subcategory->subcategory_name}}</option>
                   @endforeach
               </select>



                       <select name="priceId">
                           <option value="0" > Filter By Price </option>
                           <option value="1">Price ASC</option>
                           <option value="2">Price DESC</option>
                       </select>
   <input hidden name="category_id" type="text" value="{{$products[0]->category_id}}" />
   <button type="submit" class="btn btn-success" > search </button>
                   </div>
               </form>
               </div>
               <!-- /LIST OPTIONS -->


               <ul class="shop-item-list row list-inline m-0">

                   <!-- ITEM -->

                   @foreach ($products as $product)
                   <li class="col-lg-3">

                       <div class="shop-item">

                           <div class="thumbnail">
                               <!-- product image(s) -->
                               <a class="shop-item-image" href="/productDetails/{{$product->id}}">
                                   <img  class="img-fluid"  src="{{asset($product->main_image)}}"   alt="shop first image" />
                              </a>
                               <!-- /product image(s) -->

                               <!-- hover buttons -->
                               <div class="shop-option-over"><!-- replace data-item-id width the real item ID - used by js/view/demo.shop.js -->

                                  @if (Auth::check())


                                  <form  class="wishlistAdd" method="POST">
                                   @csrf
                               <input type="text"  name="user_id" value="{{Auth::user()->id}}"  hidden/>
                                   <input type="text" name="product_id" value="{{$product->id}}"     hidden/>
                                <button class="btn btn-light add-wishlist" type="submit"  data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart p-0"></i></button>
                               </form>





                                  @endif


                               </div>
                               <!-- /hover buttons -->

                               <!-- product more info -->
                               <div class="shop-item-info">
                                   <span class="badge badge-success">NEW</span>
                                   @if ($product->discount > 0 )
                                   <span class="badge badge-danger">SALE</span>
                                   @endif

                               </div>
                               <!-- /product more info -->
                           </div>

                           <div class="shop-item-summary text-center">
                               <h2>{{$product->product_name}}</h2>

                               <!-- rating -->
                               <div class="shop-item-rating-line">
                                   <div class="rating rating-4 fs-13"><!-- rating-0 ... rating-5 --></div>
                               </div>
                               <!-- /rating -->


                               @if ($product->discount)
                               <div class="shop-item-price">
                                   <span class="line-through">{{$product->price}}</span>
                                   {{$product->price - ($product->price*$product->discount/100)}}
                               </div>
                               @else
                               <div class="shop-item-price">
                                   {{$product->price}}

                               </div>
                               @endif


                           </div>

                               <!-- buttons -->

                               <div class="shop-item-buttons text-center">
                                   @if (Auth::check())
                                   <form  class="cartAdd" method="POST">
                                       @csrf
                                   <input type="text"  name="user_id" value="{{Auth::user()->id}}"  hidden/>
                                       <input type="text" name="product_id" value="{{$product->id}}"     hidden/>
                                       <button type="submit" class="btn btn-light" ><i class="fa fa-cart-plus"></i> Add to Cart</button>
                                   </form>
                                   @endif
                              </div>
                               <!-- /buttons -->
                       </div>

                   </li>
                   @endforeach

                   <!-- /ITEM -->

                   <!-- /ITEM -->

               </ul>

               <hr />

               <!-- Pagination Default -->
               <div class="text-center">
                   {{$products->links()}}

               </div>

            @endif

            <!-- LIST OPTIONS -->

            <!-- /Pagination Default -->


        </div>

    </div>

</section>
<!-- / -->


@endsection

@section('externalscripts')
<script>

    $( document ).ready(function() {
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(".wishlistAdd").submit(function(event){
        event.preventDefault();
        var form = $(this);
        var data = form.serialize();
        var user_id = $("input[name=user_id]").val();
        var product_id = $("input[name=product_id]").val();


        $.ajax({
          url: "/AddToWishList",
          type:"POST",
          data,
          success:function(response){
            toastr.success('Added To WishList Succesfully')

            },
            error: function(xhr, status, error) {
                console.log(xhr);
                toastr.warning('This One Exists in Your WishList')
}

         });
    });
    $(".cartAdd").submit(function(event){
        event.preventDefault();
        var form = $(this);
        var data = form.serialize();

        $.ajax({
          url: "/AddToCart",
          type:"POST",
          data,
          success:function(response){
            toastr.success('Added To Cart Succesfully')

            },
            error: function(xhr, status, error) {
                console.log(xhr);
                toastr.warning('This One Exists in Your Cart')
}

         });
    });
});

  </script>


@endsection
