@extends('welcome')

@section('title')
<title>{{Auth::user()->name}} Wishlist| Shopy's Market</title>
<meta name="description" content="Market Wishlist">
<meta name="keywords" content="shop tags, shop description, shop keywords, SEO, search engine optimization">
@endsection

@section('content')

<section class="page-header page-header-xs">
    <div class="container">

        <h1>WishList</h1>

        <!-- breadcrumbs -->


    </div>
</section>
<!-- /PAGE HEADER -->




<!-- CART -->
<section>
    <div class="container">





        <!-- CART -->
        <div class="row">

            <!-- LEFT -->
            <div class="col-lg-12 col-sm-12">

                <!-- CART -->
                <form class="cartContent clearfix" >

                    <!-- cart content -->
                    <div id="cartContent">

                        <table class="table" id="WishlistTable">
                            <thead>
                              <tr>
                                <th scope="col">#</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Transfer To Cart</th>

                                <th scope="col">Remove</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($wishlist->products as $wish)
                              <tr>
                                <th scope="row"><img src="{{asset($wish->main_image)}}" alt="" width="80" /></th>
                                <td> {{$wish->product_name}} </td>
                                <td>

                     <button type="button"  data-product="{{$wish->id}}"  data-user="{{Auth::user()->id}}"  class="btn btn-success transferProduct"> To Cart </button>

                                </td>

                                <td><button type="button" data-product="{{$wish->id}}"   class="btn btn-danger deleteProduct"> <i class="fa fa-trash"> </i> </button></td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        <!-- cart header -->

                        <!-- /cart header -->

                        <!-- cart item -->




                        <!-- /cart item -->


                        <!-- update cart -->

                        <!-- /update cart -->

                        <div class="clearfix"></div>
                    </div>
                    <!-- /cart content -->

                </form>
                <!-- /CART -->

            </div>


            <!-- RIGHT -->


        </div>
        <!-- /CART -->

    </div>
</section>


@endsection
@section('externalscripts')
    <script src="{{asset("webassets/js/wishlist.js")}}">  </script>
@endsection
