@extends('welcome')
@section('title')
<title>{{Auth::user()->name}} Cart| Shopy's Market</title>
<meta name="description" content="Market Cart">
<meta name="keywords" content="shop tags, shop description, shop keywords, SEO, search engine optimization">
@endsection

@section('content')

<section class="page-header page-header-xs">
    <div class="container">

        <h1>SHOP CART</h1>

        <!-- breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Shop</a></li>
            <li class="active">Shop Cart</li>
        </ol><!-- /breadcrumbs -->

    </div>
</section>
<!-- /PAGE HEADER -->




<!-- CART -->
<section>
    <div class="container">

@if (count($products->carts)==0)
<div class="card card-default">
    <div class="card-block">
        <strong>Shopping cart is empty!</strong><br />
        You have no items in your shopping cart.<br />
        Click <a href="index.html">here</a> to continue shopping. <br />
        <span class="badge badge-warning">this is just an empty cart example</span>
    </div>
</div>

        <!-- EMPTY CART -->

        <!-- /EMPTY CART -->


@else
        <!-- CART -->
        <div class="row">

            <!-- LEFT -->
            <div class="col-lg-9 col-sm-8">

                <!-- CART -->
                <form class="cartContent clearfix" method="post" action="#">

                    <!-- cart content -->
                    <div id="cartContent">
                        <!-- cart header -->
                        <div class="item head clearfix">
                            <span class="cart_img"></span>
                            <span class="product_name fs-13 bold">PRODUCT NAME</span>
                            <span class="remove_item fs-13 bold"></span>
                            <span class="total_price fs-13 bold">TOTAL</span>
                            <span class="qty fs-13 bold">QUANTITY</span>
                        </div>
                        <!-- /cart header -->
@foreach ($products->carts as $product)
<div class="item">
    <div class="cart_img float-left fw-100 p-10 text-left"><img src="{{$product->product->main_image}}" alt="" width="150" /></div>
    <a href="shop-single-left.html" class="product_name">
        <span>{{$product->product->product_name}}</span>

    </a>
<a href="/RemoveFromCart/{{$product->product->product_id}}" class="remove_item"><i class="fa fa-times"></i></a>
    @if($product->product->discount > 0)

    <div class="total_price">${{($product->product->price - ($product->product->price * $product->product->discount)/100)*$product->product->number}}<span></span></div>
    @else
    <div class="total_price">${{$product->product->number*$product->product->price}}<span></span></div>
    @endif
   @if ($product->product->discount > 0)
   <div class="qty"><input type="number" value="{{$product->number}}" name="qty" maxlength="3" max="999" min="1" /> &times; {{$product->product->price - ($product->product->price * $product->product->discount)/100}}</div>

   @else
   <div class="qty"><input type="number" value="{{$product->number}}" name="qty" maxlength="3" max="999" min="1" /> &times; {{$product->product->price}}</div>

   @endif
    <div class="clearfix"></div>
</div>
@endforeach

<div class="item">
    <div class="cart_img float-left fw-100 p-10 text-left"><img src="" alt="" width="150" /></div>




    <div class="total_price">${{$sum}}<span></span></div>



    <div class="clearfix"></div>
</div>
                        <!-- update cart -->
                        <button class="btn btn-success mt-20 mr-10 float-right"><i class="glyphicon glyphicon-ok"></i> UPDATE CART</button>
                        <a href="/ClearCart" class="btn btn-danger mt-20 mr-10 float-right"><i class="fa fa-remove"></i> CLEAR CART</a>
                        <!-- /update cart -->

                        <div class="clearfix"></div>
                    </div>
                    <!-- /cart content -->

                </form>
                <!-- /CART -->

            </div>
            <div class="col-lg-3 col-sm-4">


                <div class="toggle-transparent toggle-bordered-full clearfix">

                    <div class="toggle mt-0">
                        <label>Voucher</label>

                        <div class="toggle-content">
                            <p>Enter your discount coupon code.</p>

                            <form  action="/ApplyCoupon"  method="post" class="m-0" id="couponForm">
                                @csrf
                                <input type="text" id="cart-code" name="coupon_name" class="form-control text-center mb-10" placeholder="Voucher Code" required="required">
                                <button class="btn btn-primary btn-block" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>

                    <div class="toggle">
                        <label>Shipping tax calculator</label>

                        <div class="toggle-content">
                            <p>To get a shipping estimate, please enter your destination.</p>

                            <form action="#" method="post" class="m-0">
                                <label>Country*</label>
                                <select id="cart-tax-country" name="cart-tax-country" class="form-control pointer mb-20">
                                    <option value="1">United States</option>
                                    <option value="2">United Kingdom</option>
                                    <option value="2">...........</option>

                                </select>

                                <label>State/Province</label>
                                <select id="cart-tax-state" name="cart-tax-state" class="form-control pointer mb-20">
                                    <option value="1">Alabama</option>
                                    <option value="2">Alaska</option>
                                    <option value="2">...........</option>

                                </select>

                                <label>Zip/Postal Code</label>
                                <input type="text" id="cart-tax-postal" name="cart-tax-postal" class="form-control mb-20">

                                <button class="btn btn-primary btn-block" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>

                </div>


                <div class="toggle-transparent toggle-bordered-full clearfix">
                    <div class="toggle active">
                        <div class="toggle-content">

                            <span class="clearfix">
                                <span class="float-right">${{$sum}}</span>
                                <strong class="float-left">Subtotal:</strong>
                            </span>
                            <span class="clearfix">
                                <span class="float-right coupdis">$0.00</span>
                                <span class="float-left">Discount:</span>
                            </span>

                            <hr />

                            <span class="clearfix">
                                @if(Session::has('coupon'))
                                <span class="float-right fs-20 total" data-sum="{{$sum}}">${{ $sum-(  $sum  *Session::get('coupon')['discount']/100)}}</span>

                                @else
                                <span class="float-right fs-20 total" data-sum="{{$sum}}">${{$sum}}</span>

                                @endif
                                <strong class="float-left">TOTAL:</strong>
                            </span>

                            <hr />

                            <a href="/Checkout" class="btn btn-primary btn-lg btn-block fs-15"><i class="fa fa-mail-forward"></i> Proceed to Checkout</a>
                        </div>
                    </div>
                </div>

            </div>
            @endif




        </div>
        <!-- /CART -->

    </div>
</section>

@endsection

@section("externalscripts")
<script>
/*

    $( document ).ready(function() {
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("#couponForm").submit(function(e){

e.preventDefault();
var total = $(".total").data("sum");
            $.ajax(
            {
                url: "/ApplyCoupon",
                type: 'post',
               data:$("#couponForm").serialize(),



                success: function (response)
                {
                    console.log(response[0].discount);
   var s=total-(response[0].discount*total/100);
                    $(".coupdis").text("%"+response[0].discount);
                    $(".total").text(s);
                    toastr.success('Coupon Applied To Cart Succesfully')
                } ,
            error: function(xhr, status, error) {
                console.log(xhr);
                toastr.warning('There is no Coupon with this')
}
            });

          //  console.log("It failed");
        });


    });





*/
        </script>



@endsection
