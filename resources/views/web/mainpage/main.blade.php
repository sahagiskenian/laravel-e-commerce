@extends('welcome')
@section('title')
<title>Home | Shopy's Market</title>
<meta name="description" content="Market ">
<meta name="keywords" content="shop tags, shop description, shop keywords, SEO, search engine optimization">
@endsection

@php
use App\Setting;
$setting=Setting::first();
$email=json_decode($setting->emails);
$item=$email[0];
@endphp
@section('content')
    	<!-- REVOLUTION SLIDER -->
			<section id="slider" class="fullwidthbanner-container roundedcorners">
				<!--
					Navigation Styles:

						data-navigationStyle="" theme default navigation

						data-navigationStyle="preview1"
						data-navigationStyle="preview2"
						data-navigationStyle="preview3"
						data-navigationStyle="preview4"

					Bottom Shadows
						data-shadow="1"
						data-shadow="2"
						data-shadow="3"

					Slider Height (do not use on fullscreen mode)
						data-height="300"
						data-height="350"
						data-height="400"
						data-height="450"
						data-height="500"
						data-height="550"
						data-height="600"
						data-height="650"
						data-height="700"
						data-height="750"
						data-height="800"
				-->
				<div class="fullscreenbanner" data-navigationStyle="preview4">
					<ul class="hide">

						<!-- SLIDE  -->
						<li data-transition="random" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide title 1" data-thumb="{{asset($setting->slider_image1)}}">

							<img src="{{asset($setting->slider_image1)}}" data-lazyload="{{asset($setting->slider_image1)}}" alt="" data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat" />

							<div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

							<div class="tp-caption customin ltl tp-resizeme text_white"
								data-x="center"
								data-y="180"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1000"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 10;">
								<span class="fw-300"></span>
							</div>

							<div class="tp-caption customin ltl tp-resizeme large_bold_white"
								data-x="center"
								data-y="230"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1200"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 10;">
								{{$setting->slider1_text}}
							</div>



							<div class="tp-caption customin ltl tp-resizeme"
								data-x="center"
								data-y="438"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1550"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 10;">
								<a href="/contactus" class="btn btn-default btn-lg">
									<span>Purchase Shopy Now</span>
								</a>
							</div>

						</li>

						<!-- SLIDE -->
						<li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide title 2" data-thumb="{{asset($setting->slider_image2)}}">

							<img src="{{asset($setting->slider_image2)}}" data-lazyload="{{asset($setting->slider_image2)}}" alt="video" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">

							<div class="tp-caption tp-fade fadeout fullscreenvideo"
								data-x="0"
								data-y="0"
								data-speed="1000"
								data-start="1100"
								data-easing="Power4.easeOut"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1500"
								data-endeasing="Power4.easeIn"
								data-autoplay="true"
								data-autoplayonlyfirsttime="false"
								data-nextslideatend="true"
								data-volume="mute"
								data-forceCover="1"
								data-aspectratio="16:9"
								data-forcerewind="on" style="z-index: 2;">

								<div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>


							</div>



							<div class="tp-caption customin ltl tp-resizeme large_bold_white"
								data-x="center"
								data-y="230"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1200"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 3;">
								{{$setting->slider2_text}}
							</div>



							<div class="tp-caption customin ltl tp-resizeme"
								data-x="center"
								data-y="438"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
								data-speed="800"
								data-start="1550"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 3;">
								<a href="/contactus" class="btn btn-default btn-lg">
									<span>Purchase Shopy Now</span>
								</a>
							</div>

						</li>

						<!-- SLIDE  -->
						<li data-transition="random" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide title 3" data-thumb="{{asset($setting->slider_image3)}}">

							<img src="{{asset($setting->slider_image3)}}" data-lazyload="{{asset($setting->slider_image3)}}" alt="" data-bgfit="cover" data-bgposition="center bottom" data-bgrepeat="no-repeat" />

							<div class="overlay dark-4"><!-- dark overlay [1 to 9 opacity] --></div>

							<div class="tp-caption very_large_text lfb ltt tp-resizeme"
								data-x="right" data-hoffset="-100"
								data-y="center" data-voffset="-100"
								data-speed="600"
								data-start="800"
								data-easing="Power4.easeOut"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="500"
								data-endeasing="Power4.easeIn">
								{{$setting->slider3_text}}
							</div>



						</li>

					</ul>
				</div>
			</section>
			<!-- /REVOLUTION SLIDER -->



			<!-- Welcome -->
			<section>
				<div class="container">

					<div class="text-center">
						<h1>Welcome! World Of <span>Shopy</span>.</h1>
						<h2 class="col-sm-10 offset-sm-1 mb-0 fw-400">Shop With Us !</h2>
					</div>

				</div>
			</section>
			<!-- Welcome -->



			<!-- Services -->
			<section>
				<div class="container">

					<div class="row">
@foreach ($services as $service)
<div class="col-sm-6 col-md-4 col-lg-4">

    <div class="box-icon box-icon-left">
        <a class="box-icon-title" href="#">
            <i class="{{$service->logo}}"></i>
            <h2>{{$service->title}}</h2>
        </a>
        <p class="text-muted">{{$service->description}}</p>
    </div>

</div>
@endforeach


					</div>

				</div>
			</section>
			<!-- /Services -->







            <section>
				<div class="container">

					<header class="text-center mb-60">
						<h1 class="fw-300">Recent Products</h1>
						<h2 class="fw-300 letter-spacing-1 fs-13"><span>WE TRULY CARE ABOUT OUR CUSTOMERS</span></h2>
					</header>

					<!--
						controlls-over		= navigation buttons over the image
						buttons-autohide 	= navigation buttons visible on mouse hover only

						data-plugin-options:
							"singleItem": true
							"autoPlay": true (or ms. eg: 4000)
							"navigation": true
							"pagination": true
							"items": "4"

						owl-carousel item paddings
							.owl-padding-0
							.owl-padding-3
							.owl-padding-6
							.owl-padding-10
							.owl-padding-15
							.owl-padding-20
					-->
					<div class="owl-carousel owl-padding-10 buttons-autohide controlls-over" data-plugin-options='{"singleItem": false, "items":"4", "autoPlay": 4000, "navigation": true, "pagination": false}'>
                        @foreach ($products as $product)
                        <div class="img-hover">
							<a href="/productDetails/{{$product->id}}">
								<img class="img-fluid" src="{{$product->main_image}}" alt="">
							</a>

							<h4 class="text-left mt-20"><a href="/productDetails/{{$product->id}}">{{$product->product_name}}</a></h4>
							<p class="text-left">{{substr($product->product_description,0,100)}}</p>
							<ul class="text-left fs-12 list-inline list-separator">
								<li>
									<i class="fa fa-calendar"></i>
									{{$product->created_at}}
								</li>
								<li>
									<a href="blog-single-default.html#comments">
										<i class="fa fa-comments"></i>
										{{ $product->comments()->count() }}
									</a>
								</li>
							</ul>
						</div>
                        @endforeach


					</div>

				</div>
			</section>


			<!-- CALLOUT -->
			<div class="alert alert-transparent bordered-bottom m-0">
				<div class="container">

					<div class="row">

						<div class="col-md-9 col-sm-12"><!-- left text -->
							<h3>Call now at <span><strong>{{$item->phone}} </strong></span> and get 15% discount!</h3>
							<p class="font-lato fw-300 fs-20 mb-0">
								We truly care about our users and our product.
							</p>
						</div><!-- /left text -->


						<div class="col-md-3 col-sm-12 text-right"><!-- right btn -->
							<a href="/contactus" rel="nofollow" target="_blank" class="btn btn-primary btn-lg">PURCHASE NOW</a>
						</div><!-- /right btn -->

					</div>

				</div>
			</div>
			<!-- /CALLOUT -->
@endsection


