@extends('welcome')
@section('title')
<title>Contact Us | Shopy's Market</title>
<meta name="description" content="Market Contact Us descrips ">
<meta name="keywords" content="shop tags, shop description, shop keywords, SEO, search engine optimization">
@endsection
@php
use App\Setting;
$setting=Setting::first();
$email=json_decode($setting->emails);
$item=$email[0];
@endphp
@section('content')

<section class="page-header">
    <div class="container">

        <h1>CONTACT</h1>

        <!-- breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Contact</li>
        </ol><!-- /breadcrumbs -->

    </div>
</section>
<!-- /PAGE HEADER -->




<!-- -->
<section>
    <div class="container">

        <div class="row">

            <!-- FORM -->
            <div class="col-md-8">

                <h3>Drop us a line or just say <strong><em>Hello!</em></strong></h3>


                <!--
                    MESSAGES

                        How it works?
                        The form data is posted to php/contact.php where the fields are verified!
                        php.contact.php will redirect back here and will add a hash to the end of the URL:
                            #alert_success 		= email sent
                            #alert_failed		= email not sent - internal server error (404 error or SMTP problem)
                            #alert_mandatory	= email not sent - required fields empty
                            Hashes are handled by assets/js/contact.js

                        Form data: required to be an array. Example:
                            contact[email][required]  WHERE: [email] = field name, [required] = only if this field is required (PHP will check this)
                            Also, add `required` to input fields if is a mandatory field.
                            Example: <input required type="email" value="" class="form-control" name="contact[email][required]">

                        PLEASE NOTE: IF YOU WANT TO ADD OR REMOVE FIELDS (EXCEPT CAPTCHA), JUST EDIT THE HTML CODE, NO NEED TO EDIT php/contact.php or javascript
                                     ALL FIELDS ARE DETECTED DINAMICALY BY THE PHP

                        WARNING! Do not change the `email` and `name`!
                                    contact[name][required] 	- should stay as it is because PHP is using it for AddReplyTo (phpmailer)
                                    contact[email][required] 	- should stay as it is because PHP is using it for AddReplyTo (phpmailer)
                -->

                <!-- Alert Success -->
                <div id="alert_success" class="alert alert-success mb-30">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Thank You!</strong> Your message successfully sent!
                </div><!-- /Alert Success -->


                <!-- Alert Failed -->
                <div id="alert_failed" class="alert alert-danger mb-30">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>[SMTP] Error!</strong> Internal server error!
                </div><!-- /Alert Failed -->


                <!-- Alert Mandatory -->
                <div id="alert_mandatory" class="alert alert-danger mb-30">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Sorry!</strong> You need to complete all mandatory (*) fields!
                </div><!-- /Alert Mandatory -->


                <form action="/contactus" method="post" enctype="multipart/form-data">
                    @csrf
                    <fieldset>
                        <input type="hidden" name="action" value="contact_send" />

                        <div class="row">
                                <div class="col-md-12">
                                    <label for="contact:name">Full Name *</label>
                                    <input required type="text" value="" class="form-control" name="feedback_name" id="contact:name">
                                </div>
                                <div class="col-md-12">
                                    <label for="contact:email">E-mail Address *</label>
                                    <input required type="email" value="" class="form-control" name="feedback_email" id="contact:email">
                                </div>

                        </div>

                        <div class="row">
                                <div class="col-md-12">
                                    <label for="contact:message">Message *</label>
                                    <textarea required maxlength="10000" rows="8" class="form-control" name="feedback_message" id="contact:message"></textarea>
                                </div>
                        </div>


                    </fieldset>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> SEND MESSAGE</button>
                        </div>
                    </div>
                </form>

            </div>
            <!-- /FORM -->


            <!-- INFO -->
            <div class="col-md-4">

                <h2>Visit Us</h2>

                <!--
                Available heights
                    h-100
                    h-150
                    h-200
                    h-250
                    h-300
                    h-350
                    h-400
                    h-450
                    h-500
                    h-550
                    h-600
                -->
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d585491.7248582646!2d44.18285099577219!3d40.07805360976214!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x406abcde6f698f71%3A0x7eb0de99a8622af8!2sYerevan%20State%20Medical%20University!5e0!3m2!1sen!2sus!4v1608547768550!5m2!1sen!2sus"  height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

                <hr />

                <p>
                    <span class="block"><strong><i class="fa fa-map-marker"></i> Address:</strong> {{$item->address}}</span>
                    <span class="block"><strong><i class="fa fa-phone"></i> Phone:</strong> <a href="tel:{{$item->phone}}">{{$item->phone}}</a></span>
                    <span class="block"><strong><i class="fa fa-envelope"></i> Email:</strong> <a href="mailto:{{$item->email}}">{{$item->email}}</a></span>
                </p>

            </div>
            <!-- /INFO -->

        </div>

    </div>
</section>
<!-- / -->


@endsection
