@extends('welcome')
@php
use App\Setting;
    $setting=Setting::first();
@endphp
@section('title')
<title>About Us | Shopy's Market</title>
<meta name="description" content="Market About Us Page">
<meta name="keywords" content="shop tags, shop description, shop keywords, SEO, about market">
@endsection


@section('content')
<section class="page-header page-header-xlg parallax parallax-3" style="background-image:url('demo_files/images/1200x800/17-min.jpg')">
    <div class="overlay dark-5"><!-- dark overlay [1 to 9 opacity] --></div>

    <div class="container">

        <h1>ABOUT US</h1>

        <!-- breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">About Us</li>
        </ol><!-- /breadcrumbs -->

    </div>
</section>
<!-- /PAGE HEADER -->




<!-- 3 Cols -->
<section class="b-0" style="background-color:#fbfbfb;">
    <div class="container">
        <div class="row">

            <div class="col-md-4">

                <div class="heading-title heading-border-bottom heading-color">
                    <h3>Passion</h3>
                </div>

                <p>Fabulas definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii propriae philosophia cu mea. Utinam ipsum everti necessitatibus at fuisset splendide.</p>

            </div>

            <div class="col-md-4">
                <div class="heading-title heading-border-bottom heading-color">
                    <h3>Precision</h3>
                </div>
                <p>Fabulas definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii propriae philosophia cu mea. Utinam ipsum everti necessitatibus at fuisset splendide.</p>

            </div>

            <div class="col-md-4">
                <div class="heading-title heading-border-bottom heading-color">
                    <h3>Simplicity</h3>
                </div>
                <p>Fabulas definitiones ei pri per recteque hendrerit scriptorem in errem scribentur mel fastidii propriae philosophia cu mea. Utinam ipsum everti necessitatibus at fuisset splendide.</p>

            </div>

        </div>

    </div>
</section>
<!-- /3 Cols -->



<!-- Counters -->
<section class="section-sm b-0">
    <div class="container">

        <div class="row countTo-sm text-center">

            <div class="col-6 col-sm-3 col-6">
                <i class="ico-lg ico-transparent et-megaphone"></i>
                <div class="block fs-40" style="color: #3498db;">
                    <strong class="countTo fs-40" data-speed="3000">{{$setting->aboutus_stats1}}</strong>
                </div>
                <h3 class="fs-15 mt-10 mb-0">LINES OF CODE</h3>
            </div>

            <div class="col-6 col-sm-3 col-6">
                <i class="ico-lg ico-transparent et-chat"></i>
                <div class="block fs-40" style="color: #e74c3c;">
                    <strong class="countTo fs-40" data-speed="3000">{{$setting->aboutus_stats2}}</strong> %
                </div>
                <h3 class="fs-15 mt-10 mb-0">SUPPORT</h3>
            </div>

            <div class="col-6 col-sm-3 col-6">
                <i class="ico-lg ico-transparent et-flag"></i>
                <div class="block fs-40" style="color: #16a085;">
                    <strong class="countTo fs-40" data-speed="3000">{{$setting->aboutus_stats3}}</strong> %
                </div>
                <h3 class="fs-15 mt-10 mb-0">CUSTOMER CARE</h3>
            </div>

            <div class="col-6 col-sm-3 col-6">
                <i class="ico-lg ico-transparent et-puzzle"></i>
                <div class="block fs-40" style="color: #9b59b6;">
                    <strong class="countTo fs-40" data-speed="3000">{{$setting->aboutus_stats4}}</strong>
                </div>
                <h3 class="fs-15 mt-10 mb-0">DELIVERY</h3>
            </div>

        </div>

    </div>
</section>
<!-- /Counters -->




<!-- Member -->
<section class="p-0 b-0" style="background-color:#f5f6fa;">

    <div class="row m-0">

        <!-- LEFT -->
        <div class="col-md-6 col-sm-6 p-0 m-0">

            <img class="img-fluid" src="{{asset($setting->aboutus_image)}}" alt="image" />

        </div>

        <!-- RIGHT -->
        <div class="col-md-6 col-sm-6 p-0 m-0">

            <div class="p-40">

                <h2>{{$setting->aboutus_title}}</h2>
                <p>{!! nl2br(e($setting->aboutus_description)) !!}</p>


                <div class="row mt-60">

                    <div class="col-md-6 col-sm-6">
                        <p class="font-lato fs-18">Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum. Donec id elit non mi porta gravida at eget metus.</p>
                    </div>

                    <!-- skills -->
                    <div class="col-md-6 col-sm-6">

                        <h5 class="m-0"><span class="float-right">90%</span>WEB DESIGN</h5>
                        <div class="progress progress-xs"><!-- progress bar -->
                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%; min-width: 2em;"></div>
                        </div><!-- /progress bar -->

                        <h5 class="m-0"><span class="float-right">98%</span>HTML/CSS </h5>
                        <div class="progress progress-xs"><!-- progress bar -->
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100" style="width: 98%; min-width: 2em;"></div>
                        </div><!-- /progress bar -->

                    </div>
                    <!-- skills -->

                </div>

                <hr />

                <!-- social icons -->
                <div class="text-center">

                    <a href="#" class="social-icon social-icon-transparent social-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                        <i class="icon-facebook"></i>
                        <i class="icon-facebook"></i>
                    </a>

                    <a href="#" class="social-icon social-icon-transparent social-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                        <i class="icon-twitter"></i>
                        <i class="icon-twitter"></i>
                    </a>

                    <a href="#" class="social-icon social-icon-transparent social-linkedin" data-toggle="tooltip" data-placement="top" title="Linkedin">
                        <i class="icon-linkedin"></i>
                        <i class="icon-linkedin"></i>
                    </a>

                </div>
                <!-- /social icons -->

                </div>


            </div>
        </div>

    </div>

</section>
<!-- /Member -->










<!-- Clients -->
<section class="section-sm">
    <div class="container">

        <header class="text-center mb-80">
            <h3>HAPPY CLIENTS</h3>
        </header>


        <ul class="row clients-dotted list-inline">
            <li class="col-md-5th col-sm-5th col-6">
                <a href="#">
                    <img class="img-fluid" src="{{asset('brands/1.jpg')}}" alt="client" />
                </a>
            </li>
            <li class="col-md-5th col-sm-5th col-6">
                <a href="#">
                    <img class="img-fluid" src="{{asset('brands/2.jpg')}}" alt="client" />
                </a>
            </li>
            <li class="col-md-5th col-sm-5th col-6">
                <a href="#">
                    <img class="img-fluid" src="{{asset('brands/3.jpg')}}" alt="client" />
                </a>
            </li>
            <li class="col-md-5th col-sm-5th col-6">
                <a href="#">
                    <img class="img-fluid" src="{{asset('brands/4.jpg')}}" alt="client" />
                </a>
            </li>
            <li class="col-md-5th col-sm-5th col-6">
                <a href="#">
                    <img class="img-fluid" src="{{asset('brands/5.jpg')}}" alt="client" />
                </a>
            </li>
            <li class="col-md-5th col-sm-5th col-6">
                <a href="#">
                    <img class="img-fluid" src="{{asset('brands/6.jpg')}}" alt="client" />
                </a>
            </li>
            <li class="col-md-5th col-sm-5th col-6">
                <a href="#">
                    <img class="img-fluid" src="{{asset('brands/7.jpg')}}" alt="client" />
                </a>
            </li>
            <li class="col-md-5th col-sm-5th col-6">
                <a href="#">
                    <img class="img-fluid" src="{{asset('brands/8.jpg')}}" alt="client" />
                </a>
            </li>
            <li class="col-md-5th col-sm-5th col-6">
                <a href="#">
                    <img class="img-fluid" src="{{asset('brands/1.jpg')}}" alt="client" />
                </a>
            </li>
            <li class="col-md-5th col-sm-5th col-6">
                <a href="#">
                    <img class="img-fluid" src="{{asset('brands/2.jpg')}}" alt="client" />
                </a>
            </li>
        </ul>

    </div>
</section>
<!-- /Clients -->






<!-- VISUAL CHART -->
<section class="bb-0">

    <!--
        Available Classes/Colors
            .cart-visual-warning
            .cart-visual-info
            .cart-visual-success
            .cart-visual-danger
            .cart-visual-default
    -->
    <div class="clearfix mt-60 cart-visual cart-visual-default">

        <svg id="cart-visual-svg-1" height="0" width="100%" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1096 131.25" style="height: 140px;" xml:space="preserve">
            <path d="M782,49.418c-58.667-24.667-118.602-46-249.334-46s-159.43,36.157-209.333,62.207C259.04,99.186,0,131.25,0,131.25h1096C1096,131.25,840.667,74.085,782,49.418z"></path>
        </svg>

        <svg id="cart-visual-svg-2" height="0" width="100%" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1133.338 144.499" style="height: 195px;" xml:space="preserve">
            <path d="M948.868,144.499H0V72.249c26.67-9.407,66.707-19.582,119.535-19.582c133.999,0,227.999,39.333,332.665,39.333c80,0,142.667-36.666,174.667-52.666S716.201,0,804.201,0s144.667,20,144.667,20c44.173,10.997,122.713,47.543,184.47,78.215v46.284H948.868z"></path>
        </svg>

        <div class="container">

            <div class="row">

                <div class="col-6 col-md-3 mt--30 pb-180">
                    <div class="cart-visual-pin"><div class="cart-visual-pin-ball">&nbsp;</div></div>
                    <div class="pl-20 pr-20">
                        <h2><i class="countTo" data-speed="1600">{{$setting->aboutus_num1}}</i>+</h2>
                        <p class="fw--300">Happy Returning Customers</p>
                        <a href="#" class="marker-more">More +</a>
                    </div>
                </div>

                <div class="col-6 col-md-3 mt-80">
                    <div class="cart-visual-pin"><div class="cart-visual-pin-ball">&nbsp;</div></div>
                    <div class="pl-20 pr-20">
                        <h2><i class="countTo" data-speed="1300">{{$setting->aboutus_num2}}</i>%</h2>
                        <p class="fw--300">Money back gurantee</p>
                        <a href="#" class="marker-more">More +</a>
                    </div>
                </div>

                <div class="col-6 col-md-3 mt-60">
                    <div class="cart-visual-pin"><div class="cart-visual-pin-ball">&nbsp;</div></div>
                    <div class="pl-20 pr-20">
                        <h2><i class="countTo" data-speed="900">{{$setting->aboutus_num3}}</i>%</h2>
                        <p class="fw--300">Average customer growth</p>
                        <a href="#" class="marker-more">More +</a>
                    </div>
                </div>

                <div class="col-6 col-md-3 mt-0">
                    <div class="cart-visual-pin"><div class="cart-visual-pin-ball">&nbsp;</div></div>
                    <div class="pl-20 pr-20">
                        <h2><i class="countTo" data-speed="2000">{{$setting->aboutus_num4}}</i></h2>
                        <p class="fw--300">Work hours saved last 12 months</p>
                        <a href="#" class="marker-more">More +</a>
                    </div>
                </div>

            </div>

        </div>

    </div>
</section>

@endsection
