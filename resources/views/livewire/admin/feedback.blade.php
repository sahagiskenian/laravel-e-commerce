<div>
    <div class="content-body">
        <!-- Zero configuration table -->
        <section id="configuration">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Zero configuration</h4>
                  <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                  </div>
                </div>
                @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">

                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                            <th>#</th>
                          <th>  Name</th>
                          <th>  Email</th>
                          <th>Description</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                    <tbody>
                        @foreach ($feedbacks as $feedback)
                            <tr>
<td> {{$feedback->id}} </td>
<td> {{$feedback->feedback_name}} </td>
<td> {{$feedback->feedback_email}} </td>
<td> {{$feedback->feedback_message}} </td>
<td> <button wire:click="deleteFeedback({{$feedback->id}})" class="btn btn-danger"> Delete </button> </td>
                            </tr>
                        @endforeach

                    </tbody>


                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

      </div>
</div>
