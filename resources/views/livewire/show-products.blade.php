<div>
    <div class="container">

        <!-- LIST OPTIONS -->
        <div class="clearfix shop-list-options mb-20">

            <ul class="pagination m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">4</a></li>
                <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
            </ul>

            <div class="options-left">
                <select wire:model="sizeId">
                    <option value="all">Choose Size</option>
                @foreach ($sizes as $size)
                <option value="{{$size->id}}">{{$size->size_name}}</option>
                @endforeach
            </select>

            <select wire:model="subcategoryId">
                <option value="all">Choose Kind</option>
            @foreach ($subcategories as $subcategory)
            <option value="{{$subcategory->id}}">{{$subcategory->subcategory_name}}</option>
            @endforeach
        </select>



                <select>
                    <option value="0" > Filter By Price </option>
                    <option value="1">Price ASC</option>
                    <option value="2">Price DESC</option>
                </select>


            </div>

        </div>
        <!-- /LIST OPTIONS -->


        <ul class="shop-item-list row list-inline m-0">

            <!-- ITEM -->

            @foreach ($products as $product)
            <li class="col-lg-3">

                <div class="shop-item">

                    <div class="thumbnail">
                        <!-- product image(s) -->
                        <a class="shop-item-image" href="/productDetails/{{$product->id}}">
                            <img   src="{{asset($product->main_image)}}" width="150" height="150" alt="shop first image" />
                       </a>
                        <!-- /product image(s) -->

                        <!-- hover buttons -->
                        <div class="shop-option-over"><!-- replace data-item-id width the real item ID - used by js/view/demo.shop.js -->

                           @if ($auth)


                           <form  id="wishlistAdd" method="POST">
                            @csrf
                        <input type="text"  name="user_id" value="{{Auth::user()->id}}"  hidden/>
                            <input type="text" name="product_id" value="{{$product->id}}"     hidden/>
                         <button class="btn btn-light add-wishlist" type="submit"  data-toggle="tooltip" title="Add To Wishlist"><i class="fa fa-heart p-0"></i></button>
                        </form>





                           @endif


                        </div>
                        <!-- /hover buttons -->

                        <!-- product more info -->
                        <div class="shop-item-info">
                            <span class="badge badge-success">NEW</span>
                            @if ($product->discount > 0 )
                            <span class="badge badge-danger">SALE</span>
                            @endif

                        </div>
                        <!-- /product more info -->
                    </div>

                    <div class="shop-item-summary text-center">
                        <h2>{{$product->product_name}}</h2>

                        <!-- rating -->
                        <div class="shop-item-rating-line">
                            <div class="rating rating-4 fs-13"><!-- rating-0 ... rating-5 --></div>
                        </div>
                        <!-- /rating -->


                        @if ($product->discount)
                        <div class="shop-item-price">
                            <span class="line-through">{{$product->price}}</span>
                            {{$product->price - ($product->price*$product->discount/100)}}
                        </div>
                        @else
                        <div class="shop-item-price">
                            {{$product->price}}

                        </div>
                        @endif


                    </div>

                        <!-- buttons -->

                        <div class="shop-item-buttons text-center">
                            @if ($auth)
                            <form  id="cartAdd" method="POST">
                                @csrf
                            <input type="text"  name="user_id" value="{{Auth::user()->id}}"  hidden/>
                                <input type="text" name="product_id" value="{{$product->id}}"     hidden/>
                                <button type="submit" class="btn btn-light" ><i class="fa fa-cart-plus"></i> Add to Cart</button>
                            </form>
                            @endif
                       </div>
                        <!-- /buttons -->
                </div>

            </li>
            @endforeach

            <!-- /ITEM -->

            <!-- /ITEM -->

        </ul>

        <hr />

        <!-- Pagination Default -->
        <div class="text-center">
        <button class="btn btn-secondary" wire:click="newLoadId"> Load More </button>
        </div>
        <!-- /Pagination Default -->


    </div>

</div>
