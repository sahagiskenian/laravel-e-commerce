<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\feedback;
use App\Repository\Interfaces\FeedbackInterface;
use Alert;
class FeedbacksController extends Controller
{

    private $feedbackRepository;

    public function __construct(FeedbackInterface $feedbackRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
    }

    public function index(){
      $feedbacks=$this->feedbackRepository->getFeedbacks();


      return view('admin.feedbacks')
      ->with("feedbacks",$feedbacks);
    }


    public function RemoveFeedback($id){
        $feedbacks=$this->feedbackRepository->DeleteFeedback($id);

        Alert::success('Feedback Deleted ', 'Success Message');
       return redirect()->back();
      }
}
