<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\category;
Use Alert;
use App\Repository\Interfaces\CategoryInterface;
class CategoryController extends Controller
{
    //
    private $categoryRepository;

    public function __construct(CategoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }
    public function index(){
  $category=$this->categoryRepository->getCategories();

  return view('admin.categories')->with('categories',$category);
    }
    public function store(Request $request){


     $this->categoryRepository->AddCategory($request);


         return redirect()->back();


    }



    public function RemoveCategory($id){
      $category=$this->categoryRepository->DeleteCategory($id);




      return redirect()->back();



    }
}
