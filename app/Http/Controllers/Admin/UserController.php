<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Alert;
use App\Repository\Interfaces\UserInterface;
class UserController extends Controller
{
    //
    private $UserRepository;

    public function __construct(UserInterface $UserRepository)
    {
        $this->UserRepository = $UserRepository;
    }
public function index(){
return $this->UserRepository->getAdmins();
}
public function indexUsers(){
    return $this->UserRepository->getUsers();
  }

    public function AddAdmin(Request $request) {

        return $this->UserRepository->AddAdmin($request);
     }

     public function RemoveAdmin($id){
return $this->UserRepository->RemoveAdmin($id);
     }
     public function RemoveUser($id){
        return $this->UserRepository->RemoveUser($id);
          }
}
