<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\product;
use App\category;
use App\size;
use App\subcategory;
use App\productImage;


use RealRashid\SweetAlert\Facades\Alert  ;

class ProductController extends Controller
{
    //

    public function index(){

       $category=category::all();
       $subcategory=subcategory::all();
       $size=size::all();

        return view('admin.products.addproducts')
        ->with('categories',$category)
        ->with('subcategories',$subcategory)
        ->with('sizes',$size);
    }

    public function storeProduct(Request $request){
      if($request->id ==null)
      {
      $product=new product();
      }
      else {
          $product=product::find($request->id);
      }
  $category_id=subcategory::find($request->subcategory_id)->category_id;
  $product->category_id=$category_id;
      $product->product_name=$request->product_name;
      $product->product_description=$request->product_description;

      $product->subcategory_id=$request->subcategory_id;
      $product->quantity=$request->quantity;
      $product->price=$request->price;
      $product->discount=$request->discount;
      $product->product_code=$request->product_code;

      $featured = $request->main_image;
      if($featured){
      $featured_new_name = time().$featured->getClientOriginalName();
      $featured->move('uploads/products',$featured_new_name);
      $product->main_image='uploads/products/'.$featured_new_name;
      }

      $product->save();
      $images = $request->file('images');


      if($request->hasFile('images'))
      {
          foreach ($images as $image) {
            $featured_new_name = rand(1,10000000000).".".$image->getClientOriginalExtension();
            $image->move('uploads/products',$featured_new_name);
            $product->main_image='uploads/products/'.$featured_new_name;
            $productImage =new productImage();

            $productImage->product_image= 'uploads/products/'.$featured_new_name;
            $productImage->product_id=$product->id;
            $productImage->save();
          }
        }
        $product->sizes()->attach($request->sizes);
        $product->save();

        Alert::success('Product Done', 'Success Message');

      return redirect()->back();
     }

     public function loadsubCategories($id){

        $subcategory=subcategory::where('category_id','=',$id)->get();


    return response()->json([$subcategory ], 200);
     }

     public function products($id){
         $products=product::with("category")->where('category_id','=',$id)->get();


         return view('admin.products.products')
                ->with("products",$products);
     }


     public function deleteproduct($id){
        $products=product::find($id)->delete();

        Alert::success('Product Deleted', 'Success Message');
      return redirect()->back();
    }
    public function deleteproductImage($id){
        $products=productImage::find($id)->delete();

        Alert::success('Product Image Deleted', 'Success Message');
      return redirect()->back();
    }
    public function topRatedProduct($id){
        $product=product::find($id);
        if($product->top_rated==0)
        $product->top_rated=1;
        else
        $product->top_rated=0;

        $product->save();
        Alert::success('Operation Done', 'Success Message');
      return redirect()->back();
    }


    public function hideProduct($id){
        $product=product::find($id);
        $product->hide=1;

        $product->save();
        Alert::success('Operation Done', 'Success Message');
      return redirect()->back();
    }

    public function EditProduct($id){
        $product=product::with("sizes")->with('productImages')->where('id','=',$id)->first();



        return view('admin.products.editproducts')
        ->with("product",$product)
        ->with("categories",category::all())
        ->with("subcategories",subcategory::all())
        ->with("sizes",size::all());

    }
    //products quantity less than 10
    public function ProductMin(){
        $product=product::where("quantity","<",10)->get();



        return view("admin.products.minproducts")
              ->with('products',$product);

    }
}
