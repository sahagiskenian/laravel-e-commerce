<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\subcategory;
use App\category;
use App\Repository\Interfaces\SubCategoryInterface;
use App\Repository\Interfaces\CategoryInterface;
Use Alert;
class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $subcategoryRepository;
    private $categoryRepository;

    public function __construct(SubCategoryInterface $subcategoryRepository,CategoryInterface $categoryRepository)
    {
        $this->subcategoryRepository = $subcategoryRepository;
        $this->categoryRepository = $categoryRepository;
    }
    public function index(){
        $subcategory=$this->subcategoryRepository->getSubCategories();
        $category=$this->categoryRepository->getCategories();

        return view('admin.subcategories')
        ->with('subcategories',$subcategory)
        ->with('categories',$category);
          }
          public function store(Request $request){

            $this->subcategoryRepository->AddSubCategory($request);


               return redirect()->back();


          }



          public function RemoveSubCategory($id){
            $category=subcategory::find($id)->delete();


            Alert::success('subcategory Deleted ', 'Success Message');

            return redirect()->back();



          }
}
