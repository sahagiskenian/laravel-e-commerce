<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
use Alert;
class SettingController extends Controller
{
    //
    public function contactSettings(){
        $setting=Setting::first();
        $emails=json_decode($setting->emails);


        return view("admin.settings.contactSettings")
                ->with("emails",$emails);
    }

    public function EditContactSettings(Request $request){
        $ss=$request->toArray();


        $emails=json_encode($request->car);

        $setting=Setting::first();


        $setting->emails=$emails;

        $setting->save();
        return redirect()->back();
    }

    public function aboutusStats(){
        $setting=Setting::first();

        return view("admin.settings.aboutusStats")
              ->with("setting",$setting);
    }

    public function aboutusEdit(Request $request){
        $setting=Setting::first();
        $setting->facebook_url=$request->facebook_url;
        $setting->aboutus_stats1=$request->aboutus_stats1;
        $setting->aboutus_stats2=$request->aboutus_stats2;
        $setting->aboutus_stats3=$request->aboutus_stats3;
        $setting->aboutus_stats4=$request->aboutus_stats4;
        $setting->aboutus_title=$request->aboutus_title;
        $setting->aboutus_description=$request->aboutus_description;
        $setting->aboutus_num1=$request->aboutus_num1;
        $setting->aboutus_num2=$request->aboutus_num2;
        $setting->aboutus_num3=$request->aboutus_num3;
        $setting->aboutus_num4=$request->aboutus_num4;
        $featured = $request->aboutus_image;
      if($featured){
      $featured_new_name = time().$featured->getClientOriginalName();
      $featured->move('uploads/settings',$featured_new_name);
      $setting->aboutus_image='uploads/settings/'.$featured_new_name;
      }


        $setting->save();

        Alert::success("Edited Setting Succesfully");

        return redirect()->back();
    }

    public function mainpageSetting(){
        $setting=Setting::first();

        return view("admin.settings.mainpageSettings")
              ->with("setting",$setting);
    }
    public function editmainpageSetting(Request $request){
        $setting=Setting::first();

        $setting->slider1_text=$request->slider1_text;
        $setting->slider2_text=$request->slider2_text;
        $setting->slider3_text=$request->slider3_text;

        $featured = $request->slider_image1;
        if($featured){
        $featured_new_name = time().$featured->getClientOriginalName();
        $featured->move('uploads/settings',$featured_new_name);
        $setting->slider_image1='uploads/settings/'.$featured_new_name;
        }
        $featured = $request->slider_image2;
        if($featured){
        $featured_new_name = time().$featured->getClientOriginalName();
        $featured->move('uploads/settings',$featured_new_name);
        $setting->slider_image2='uploads/settings/'.$featured_new_name;
        }
        $featured = $request->slider_image3;
        if($featured){
        $featured_new_name = time().$featured->getClientOriginalName();
        $featured->move('uploads/settings',$featured_new_name);
        $setting->slider_image3='uploads/settings/'.$featured_new_name;
        }
        $featured = $request->logo;
        if($featured){
        $featured_new_name = time().$featured->getClientOriginalName();
        $featured->move('uploads/settings',$featured_new_name);
        $setting->logo='uploads/settings/'.$featured_new_name;
        }
$setting->save();
        Alert::success("Edited Setting Succesfully");

        return redirect()->back();
    }

}
