<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\size;
Use Alert;
use App\Repository\Interfaces\SizeInterface;
class SizeController extends Controller
{
    //
    private $sizeRepository;

    public function __construct(SizeInterface $sizeRepository)
    {
        $this->sizeRepository = $sizeRepository;
    }
    public function index(){
  $size=$this->sizeRepository->getSizes();

  return view('admin.sizes')->with('sizes',$size);
    }
    public function store(Request $request){

        $this->sizeRepository->AddSize($request);


         return redirect()->back();


    }



    public function RemoveSize($id){
      $size=$this->sizeRepository->DeleteSize($id);




      return redirect()->back();



    }
}
