<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repository\Interfaces\AdminReportsInterface;
class StatisticsController extends Controller
{
    //
    private $adminReportsRepository;

    public function __construct(AdminReportsInterface $adminReportsRepository)
    {
        $this->adminReportsRepository = $adminReportsRepository;
    }
    public function index(){
        $todayOrders=$this->adminReportsRepository->todayOrders();
        $monthOrders=$this->adminReportsRepository->monthOrders();
        $yearOrders=$this->adminReportsRepository->yearOrders();
        $todayDelivered=$this->adminReportsRepository->todayDelivered();
        $orderReturned=$this->adminReportsRepository->orderReturned();
        $usersNumber=$this->adminReportsRepository->usersNumber();
        $categoriesNumber=$this->adminReportsRepository->categoriesNumber();
        $subcategoriesNumber=$this->adminReportsRepository->subcategoriesNumber();
        return view("admin.statistics.stats")
              ->with("todayOrders",$todayOrders)
              ->with("monthOrders", $monthOrders)
              ->with( "yearOrders",$yearOrders)
              ->with( "todayDelivered",$todayDelivered)
              ->with( "orderReturned",$orderReturned)
              ->with("usersNumber", $usersNumber)
              ->with("categoriesNumber", $categoriesNumber)
              ->with("subcategoriesNumber", $subcategoriesNumber);
    }
}
