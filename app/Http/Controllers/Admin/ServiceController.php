<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service;
class ServiceController extends Controller
{
    //
    public function index(){
     $services=Service::all();

     return view("admin.services.services")
              ->with("services",$services);
    }

    public function create(Request $request){
       $service=Service::find($request->id);
       if($service){
          $service->title=$request->title;
          $service->description=$request->description;
          $service->logo=$request->logo;
          $service->save();
        return redirect()->back();
       }

        $services=Service::create($request->all());

        return redirect()->back();
       }




       public function destroy($id){
        $services=Service::find($id)->delete();

        return redirect()->back();
       }
}
