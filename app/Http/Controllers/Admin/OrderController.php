<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\order;
use App\orderDetail;
use App\shipping;
use App\product;
Use Alert;
use Illuminate\Support\Facades\Mail;
use App\Mail\PaymentAcceptedEmail;
use App\Mail\PaymentRefusedEmail;
use DB;
class OrderController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index($id){
        $orders=order::where('status',$id)->get();


        return view('admin.orders.pendingorders')
        ->with('orders',$orders)
        ->with('status',$id);
    }


    public function orderDetails($id){
        $orderUser=order::where('id',$id)->with('user')->first();

        $shipping=shipping::where('order_id',$id)->first();

        $orderProduct=orderDetail::where("order_id",$id)->with('product')->get();


        return view('admin.orders.orderDetails')
                 ->with('orderUser',$orderUser)
                 ->with('shipping',$shipping)
                 ->with('orderProduct',$orderProduct)
                 ->with('status',$orderUser->status);
    }

    public function AcceptPayment($id){
        order::where('id',$id)->update(['status'=>1]);
        $shipping=shipping::where('order_id',$id)->first();
        Mail::to($shipping->ship_email)->send(new PaymentAcceptedEmail());


        Alert::success('Payment Accepted', 'Success Message');


        return redirect()->route("orders",['id'=>1]);
    }
    public function RefuseOrder($id){
        order::where('id',$id)->update(['status'=>4]);
        $shipping=shipping::where('order_id',$id)->first();
        Mail::to($shipping->ship_email)->send(new PaymentRefusedEmail());

        Alert::success('Payment Refused', 'Success Message');


        return redirect()->route("orders",['id'=>4]);
    }
    public function ProcessOrder($id){
        order::where('id',$id)->update(['status'=>2]);
        $shipping=shipping::where('order_id',$id)->first();

        Alert::success('Gone To Process');


        return redirect()->route("orders",['id'=>2]);
    }
    public function DeliverOrder($id){
        $product = orderDetail::where('order_id',$id)->get();
        foreach ($product as $row) {
         product::where('id',$row->product_id)
                ->update(['quantity' => DB::raw('quantity-'.$row->quantity)]);
        }


        order::where('id',$id)->update(['status'=>3]);
        $shipping=shipping::where('order_id',$id)->first();

        Alert::success('Order Delivered!');


        return redirect()->route("orders",['id'=>3]);
    }
    public function PendingReturnOrder(){
     $orders=   order::where("return_order",1)->get();



        return view("admin.orders.pendingreturnorders")
                    ->with("orders",$orders)
                    ->with("status",1)  ;
    }
    public function ReturnedOrders(){
        $orders=   order::where("return_order",2)->get();



           return view("admin.orders.pendingreturnorders")
                       ->with("orders",$orders)
                       ->with("status",2 )  ;
       }
       public function ReturnOrder($id){
        order::where('id',$id)->update(['return_order'=>2]);

        Alert::success('Order Returned Succesfully!');
//send email
           return Redirect()->route("PendingReturnOrder")  ;
       }
}
