<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\size;
use App\product;
use App\Comment;
use App\Repository\Interfaces\ShopInterface;
use App\Repository\Interfaces\ProductInterface;
class ShopController extends Controller
{
    //
    private $shopRepository,$productRepository;

    public function __construct(ShopInterface $shopRepository,ProductInterface $productRepository)
    {
        $this->shopRepository = $shopRepository;
        $this->productRepository = $productRepository;
    }
    public function index(){
      $sizes= $this->shopRepository->getSizes();

        return view('web.shop.shop')
        ->with("sizes",$sizes);
}
public function productDetails($id){

    $product=$this->shopRepository->getProduct($id);

$productAvg=$this->shopRepository->productAverage($id);

$relatedProducts= $this->productRepository->getRelatedProducts($product->category->id,$product->id);
    return view('web.shop.productDetails')
    ->with("product",$product)
    ->with('Rate',$productAvg)
    ->with("relatedproducts",$relatedProducts);
}
public function AddComment(Request $request){

   return  $this->shopRepository->AddComment($request);

        }

}
