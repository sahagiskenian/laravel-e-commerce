<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Charge;
use Auth;
use DB;
use Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\PaymentDoneEmail;
use App\Repository\Interfaces\ShopInterface;
class PaymentController extends Controller
{

    private $shopRepository,$productRepository;

    public function __construct(ShopInterface $shopRepository )
    {
        $this->shopRepository = $shopRepository;

    }



    public function index(){


     return view('web.shop.payment');
}

    public function PaymentProcess(Request $request){

     return $this->shopRepository->ProcessPayment($request);
    }


}
