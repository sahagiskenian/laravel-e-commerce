<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
use Alert;
use App\Repository\Interfaces\CartInterface;

use Session;
class CartController extends Controller
{
    //
    private $cartRepository;

    public function __construct(CartInterface $cartRepository)
    {
        $this->cartRepository = $cartRepository;
        $this->middleware('auth');
    }
   public function index(){
  $result=  $this->cartRepository->getMyCart();
  $sum=$this->cartRepository->CountPrice();

    return view("web.cart.buyingCart")
    ->with("products",$result)
    ->with("sum",$sum);
   }
    public function AddToCart(Request $request){
     return    $this->cartRepository->AddToCart($request);

                    }
          public function RemoveFromCart($id){

            $this->cartRepository->RemoveFromCart($id);

                    return redirect()->back();

                }

                public function ClearCart(){
                   $this->cartRepository->ClearCart();

                    return redirect()->back();
                }
                public function ApplyCoupon(Request $request){

               return    $this->cartRepository->CouponApplied($request->coupon_name);


                 }
                 public function checkout(){



           return view('web.shop.checkout');
               }
                 public function finalcheckout(Request $request){
                     $sum=$this->cartRepository->CountPrice();
                    if(Session::has('coupon')){
                        $sum=      $sum-(  $sum  *Session::get('coupon')['discount']/100);
                    }
                    $data = array();
                    $data['name'] = $request->ship_name;
                    $data['phone'] = $request->ship_phone;
                    $data['email'] = $request->ship_email;
                    $data['address'] = $request->ship_address;
                    $data['city'] = $request->ship_city;
                    return view('web.shop.finalcheckout')
                    ->with('sum',$sum)
                    ->with('data',$data);
                }
}
