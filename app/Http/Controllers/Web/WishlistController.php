<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\product;
use DB;
use Auth;
use Alert;
use App\Repository\Interfaces\WishlistInterface;
use App\Repository\Interfaces\CartInterface;
class WishlistController extends Controller
{
    //
    private $wishlistRepository;
    private $cartRepository;
    public function __construct(WishlistInterface $wishlistRepository,CartInterface $cartRepository)
    {
        $this->wishlistRepository = $wishlistRepository;
        $this->cartRepository = $cartRepository;
    }
    public function index() {

$wishlist=$this->wishlistRepository->getMyWishList();

     return view('web.cart.cart')
       ->with('wishlist',$wishlist);

    }

    public function AddToWishList(Request $request){

 return $this->wishlistRepository->AddToMyWishlist($request);

            }
           public function RemoveFromWishList($product_id){

                return $this->wishlistRepository->RemoveFromWishlist($product_id);

            }


                  public function WishlistToCart(Request $request){

                      return $this->cartRepository->AddToCart($request);

                    }
}
