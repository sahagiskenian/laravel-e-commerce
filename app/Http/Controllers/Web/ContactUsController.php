<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alert;
use App\feedback;
use App\Repository\Interfaces\FeedbackInterface;
class ContactUsController extends Controller
{
    //
    private $feedbackRepository;

    public function __construct(FeedbackInterface $feedbackRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
    }
    public function index(){

    return view('web.contactus.contactpage');
    }

    public function addFeedback(Request $request){

     return    $this->feedbackRepository->AddFeedback($request->all());


        }
}
