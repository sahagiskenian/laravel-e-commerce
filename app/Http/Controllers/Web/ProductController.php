<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\product;
use App\Repository\Interfaces\SubCategoryInterface;
use App\Repository\Interfaces\SizeInterface;
use App\Repository\Interfaces\ProductInterface;

use App\subcategory;
use App\size;
class ProductController extends Controller
{
    //
    private $sizeRepository,$subcategoryRepository,$productRepository;

    public function __construct(SizeInterface $sizeRepository,SubCategoryInterface $subcategoryRepository,ProductInterface $productRepository)
    {
        $this->sizeRepository = $sizeRepository;
        $this->subcategoryRepository = $subcategoryRepository;
        $this->productRepository = $productRepository;
    }
    public function GetCategorizedProducts($id){
        $sizes=  $this->sizeRepository->getSizes();
        $subcategories=$this->subcategoryRepository->getsubcategories($id);
   $products=$this->productRepository->getProductsCategorized($id);



   return view('web.shop.shop')
   ->with("products",$products)
   ->with("subcategories",$subcategories)
   ->with("sizes",$sizes);

    }

    public function filterProducts(Request $request){

    $response= $this->productRepository->getFilteredProducts($request);
    $sizes=  $this->sizeRepository->getSizes();
    $subcategories=$this->subcategoryRepository->getsubcategories(14);
return view('web.shop.shop')
->with("products",$response)
->with("subcategories",$subcategories)
->with("sizes",$sizes);

    }
}
