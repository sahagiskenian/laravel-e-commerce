<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Subscribtion;
class SubscribeController extends Controller
{
    //
    public function store(Request $request){
     $count= Subscribtion::where("email",$request->email)->count();
    if($count==0){
        $Subscribtion=new Subscribtion();
        $Subscribtion->email=$request->email;
        $Subscribtion->save();
        return response('Completed',200);
    }
    else {
        return response('Error',500);
    }

    }
}
