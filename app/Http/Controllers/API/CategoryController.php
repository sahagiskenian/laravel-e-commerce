<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repository\Interfaces\CategoryInterface;
class CategoryController extends Controller
{
    //
    private $categoryRepository;

    public function __construct(CategoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }
    public function index(){
  $category=$this->categoryRepository->getCategories()->toArray();
  $array=[
    'categories'=> $category,
    'Success'=>200,
    'Message'=>"Categories Retreived Succesfully"
  ];
  return response()->json($array,200);
    }

    public function getFeaturedCategories(){
        $category=$this->categoryRepository->getFeaturedCategories()->toArray();
        $array=[
          'featured_categories'=> $category,
          'Success'=>200,
          'Message'=>"Featured Categories Retreived Succesfully"
        ];
        return response()->json($array,200);
          }

          public function getCategoryProducts($id){
            $category=$this->categoryRepository->categoryProducts($id)->toArray();
            $array=[
              'category_products'=> $category,
              'Success'=>200,
              'Message'=>"  Categories Products Retreived Succesfully"
            ];
            return response()->json($array,200);
              }

}
