<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\Interfaces\SubCategoryInterface;

Use Alert;
class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $subcategoryRepository;


    public function __construct(SubCategoryInterface $subcategoryRepository )
    {
        $this->subcategoryRepository = $subcategoryRepository;

    }
    public function index(){
        $subcategory=$this->subcategoryRepository->getSubCategories()->toArray();
        $array=[
            'subcategories'=> $subcategory,
            'Success'=>200,
            'Message'=>"SubCategories Retreived Succesfully"
          ];
          return response()->json($array,200);
          }
}
