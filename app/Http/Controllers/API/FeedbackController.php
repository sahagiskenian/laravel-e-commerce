<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\Interfaces\FeedbackInterface;
class FeedbackController extends Controller
{
    //
    private $feedbackRepository;

    public function __construct(FeedbackInterface $feedbackRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
    }
    public function AddFeedback(Request $request){
    $feedback=  $this->feedbackRepository->AddFeedback($request->all());

      $response=[
     'feedback'=>$feedback,
     'status'=>200,
      'message'=>"feedback retrived Successfully"

     ];
      return response($response,200);

        }

}
