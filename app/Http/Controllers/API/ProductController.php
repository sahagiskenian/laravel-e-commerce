<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\product;
use App\Repository\Interfaces\ProductInterface;
class ProductController extends Controller
{
    //
    private $productRepository;

    public function __construct(ProductInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }
    public function index(){
        $products=$this->productRepository->getProducts()->toArray();
        $array=[
            'products'=> $products,
            'Success'=>200,
            'Message'=>"products Retreived Succesfully"
          ];
          return response()->json($array,200);
          }

          public function getLastProducts(){
            $products=$this->productRepository->getLastProducts()->toArray();
            $array=[
                'products'=> $products,
                'Success'=>200,
                'Message'=>"last products Retreived Succesfully"
              ];
              return response()->json($array,200);

          }

          public function    getRelatedProducts($subcategory_id,$id){
            $products=$this->productRepository->getRelatedProducts($subcategory_id,$id)->toArray();
            $array=[
                'products'=> $products,
                'Success'=>200,
                'Message'=>"last products Retreived Succesfully"
              ];
              return response()->json($array,200);
          }

          public function getProduct($id){
            $products=$this->productRepository->getProduct($id)->toArray();
            $productRatingAvg=$this->productRepository->getProductAvgRate($id);
            $productRatingCount=$this->productRepository->getProductRateCount($id);
            $array=[
                'products'=> $products,
                'rating_avg'=> $productRatingAvg,
                'rating_count'=>  $productRatingCount,
                'Success'=>200,
                'Message'=>" product Retreived Succesfully"
              ];
              return response()->json($array,200);

          }
          public function getCategorizedProducts($id){
            $products=product::with("productImages")->with('category')->with("subcategory")->with("sizes")->with("comments")->where("category_id",$id)->orderBy('id', 'desc')->take(3)->get()->toArray();
            $array=[
                'products'=> $products,
                'Success'=>200,
                'Message'=>" product Retreived Succesfully"
              ];
              return response()->json($array,200);

          }

}
