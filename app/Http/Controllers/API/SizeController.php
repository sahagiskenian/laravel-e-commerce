<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\Interfaces\SizeInterface;
class SizeController extends Controller
{
    //
    private $sizeRepository;

    public function __construct(SizeInterface $sizeRepository)
    {
        $this->sizeRepository = $sizeRepository;
    }
    public function index(){
        $size=$this->sizeRepository->getSizes()->toArray();
        $array=[
            'sizes'=> $size,
            'Success'=>200,
            'Message'=>"Sizes Retreived Succesfully"
          ];
          return response()->json($array,200);
          }
}
