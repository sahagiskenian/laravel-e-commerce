<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\Interfaces\AuthInterface;

class AuthController extends Controller
{
    //
    private $authRepository;

    public function __construct(AuthInterface $authRepository)
    {
        $this->authRepository = $authRepository;
    }
    public function AddUser(Request $request){

     return  $this->authRepository->Register($request->all());

    }

    public function Login(Request $request){

        return  $this->authRepository->Login($request->all());

       }

       public function logout (Request $request){
        return  $this->authRepository->logout($request);
       }

}
