<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Alert;
use Hash;
use App\User;
use Auth;
use DB;
use App\order;
use App\Repository\Interfaces\ProfileInterface;
class ProfileController extends Controller
{
    //
    private $profileRepository;

    public function __construct(ProfileInterface $profileRepository)
    {
        $this->profileRepository = $profileRepository;
    }



    public function changePassword(Request $request){

        return  $this->profileRepository->changePassword($request);
    }

    public function checkOrder(Request $request){

 return  $this->profileRepository->checkOrder($request);


    }


    public function returnOrder($id){
        return  $this->profileRepository->returnOrder($id);

    }
}
