<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\Interfaces\WishlistInterface;
class WishlistController extends Controller
{
    //
    private $wishlistRepository;

    public function __construct(WishlistInterface $wishlistRepository)
    {
        $this->wishlistRepository = $wishlistRepository;
    }

    public function GetMyWishlist(){
      $wishlist=  $this->wishlistRepository->GetMyWishlist();
      $array=[
          'wishlist'=>$wishlist,
          'status'=>200,
          'message'=>'Wishlist Retrived Sucessfuly'
      ];
      return response($array,200);
    }

    public function AddToWishlist(Request $request){

      return  $this->wishlistRepository->AddToMyWishlist($request);


      }

      public function RemoveFromWishlist($id){
      $wishlist=  $this->wishlistRepository->RemoveFromWishlist($id);
      $array=[
          'product'=>$wishlist,
          'status'=>200,
          'message'=>'Removed Product From Wishlist Sucessfuly'
      ];
      return response($array,200);
    }

}
