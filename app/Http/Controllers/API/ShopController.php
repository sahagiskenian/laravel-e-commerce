<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\size;
use App\product;
use App\Comment;
use App\Repository\Interfaces\ShopInterface;
use App\Repository\Interfaces\ProductInterface;
class ShopController extends Controller
{
    //
    private $shopRepository,$productRepository;

    public function __construct(ShopInterface $shopRepository,ProductInterface $productRepository)
    {
        $this->shopRepository = $shopRepository;
        $this->productRepository = $productRepository;
    }


public function AddComment(Request $request){

   return  $this->shopRepository->AddComment($request);

        }

}
