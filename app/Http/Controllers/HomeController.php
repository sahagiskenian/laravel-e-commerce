<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\Interfaces\ProductInterface;
use App\Service;
class HomeController extends Controller
{




        //
        private $productRepository;

        public function __construct(ProductInterface $productRepository)
        {

            $this->productRepository = $productRepository;
        }
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function mainPage(){
$products= $this->productRepository->getLastProducts();
$services=Service::all();

        return view("web.mainpage.main")
        ->with("products",$products)
        ->with("services",$services);
    }

     public function home(){
        return redirect()->route("main");
    }
}
