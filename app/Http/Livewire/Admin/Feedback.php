<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Repository\Interfaces\FeedbackInterface;

class Feedback extends Component
{
    private  $feedbackRepository;
    public $feedbacks;


    public function mount(FeedbackInterface $feedbackRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
       $this->feedbacks=$this->feedbackRepository->getFeedbacks();
    }
    public function deleteFeedback($id, FeedbackInterface $feedbackRepository){
        $this->feedbackRepository = $feedbackRepository;

    $feedback=    $this->feedbackRepository->DeleteFeedback($id);


        session()->flash('message', 'Feedback Succesfully Deleted');

       }

    public function render(FeedbackInterface $feedbackRepository)
    {
        $this->feedbacks=$this->feedbackRepository->getFeedbacks();
        return view('livewire.admin.feedback');
    }
}
