<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\product;
use App\size;
use App\subcategory;
use Livewire\WithPagination;
class ShowProducts extends Component
{

    use WithPagination;

    public $products;
    public $subcategories;
    public $sizes;
    public $sizeId,$subcategoryId;
    public $categoryId;
    public $loadId;
    public $auth;
    public function mount($id,$auth)
    {

        $this->products=product::with("productImages")->where('category_id','=',$id)->take(1)->get();
       $this->sizes=size::all();
       $this->subcategories=subcategory::all();
       $this->categoryId=$id;
       $this->subcategoryId="all";
       $this->sizeId="all";
       $this->loadId=1;
       $this->auth=$auth;
    }
    public function newLoadId()
    {  $this->loadId++;
        $sizeId=$this->sizeId;
        $this->products=  product::with("productImages")->where('category_id','=',$this->categoryId)->take($this->loadId)->get();
        if($this->sizeId=="all" && $this->subcategoryId=="all"){
            $this->products=product::with("productImages")->where('category_id','=',$this->categoryId)->take($this->loadId)->get();
        }
        else if ($this->sizeId=="all"){
            $this->products=product::with("productImages")->where('category_id','=',$this->categoryId)->where("subcategory_id",$this->subcategoryId)->take($this->loadId)->get();
        }
        else if ($this->subcategoryId=="all"){
            $this->products=product::with("productImages")->where('category_id','=',$this->categoryId)->whereHas('sizes', function($q) use($sizeId) {
                $q->where('size_id','=', $sizeId);
            })->take($this->loadId)->get();
        }
        else {
            $this->products=product::with("productImages")->where('category_id','=',$this->categoryId)->where("subcategory_id",$this->subcategoryId)->whereHas('sizes', function($q) use($sizeId) {
                $q->where('size_id','=', $sizeId);
            })->take($this->loadId)->get();
        }

    }
    public function updatedSizeId($sizeId)
    {
        if($sizeId=="all" && $this->subcategoryId=="all"){
            $this->products=product::with("productImages")->where('category_id','=',$this->categoryId)->take($this->loadId)->get();
        }
        else if ($sizeId=="all"){
            $this->products=product::with("productImages")->where('category_id','=',$this->categoryId)->where("subcategory_id",$this->subcategoryId)->take($this->loadId)->get();
        }
        else if ($this->subcategoryId=="all"){
            $this->products=product::with("productImages")->where('category_id','=',$this->categoryId)->whereHas('sizes', function($q) use($sizeId) {
                $q->where('size_id','=', $sizeId);
            })->take($this->loadId)->get();
        }
        else {
            $this->products=product::with("productImages")->where('category_id','=',$this->categoryId)->where("subcategory_id",$this->subcategoryId)->whereHas('sizes', function($q) use($sizeId) {
                $q->where('size_id','=', $sizeId);
            })->take($this->loadId)->get();
        }


    }
    public function updatedSubCategoryId($subcategoryId)
    {
         $sizeId=$this->sizeId;
        if($this->sizeId=="all" && $subcategoryId=="all"){
            $this->products=product::with("productImages")->where('category_id','=',$this->categoryId)->take($this->loadId)->get();
        }
        else if ($this->sizeId=="all"){
            $this->products=product::with("productImages")->where('category_id','=',$this->categoryId)->where("subcategory_id",$subcategoryId)->take($this->loadId)->get();
        }
        else if ($subcategoryId=="all"){
            $this->products=product::with("productImages")->where('category_id','=',$this->categoryId)->whereHas('sizes', function($q) use($sizeId) {
                $q->where('size_id','=',$sizeId);
            })->take($this->loadId)->get();
        }
        else {
            $this->products=product::with("productImages")->where('category_id','=',$this->categoryId)->where("subcategory_id",$subcategoryId)->whereHas('sizes', function($q) use($sizeId) {
                $q->where('size_id','=', $sizeId);
            })->take($this->loadId)->get();
        }

    }



    public function render()
    {
        return view('livewire.show-products');
    }
}
