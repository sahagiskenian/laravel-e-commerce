<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orderDetail extends Model
{
    //


    public function product()
    {
        return $this->belongsTo('App\product');
    }
    public function order()
    {
        return $this->belongsTo('App\order');
    }
}
