<?php

namespace App\Repository\Main;


use App\Repository\Interfaces\CategoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\category;
use App\product;
use Alert;
class CategoryRepository implements CategoryInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(category $model)
    {
        $this->model = $model;
    }

   /**
    * @return Collection
    */
   public function getCategories()
   {
       return $this->model->with("products")->get();
   }
   public function categoryProducts($id){
return product::with("sizes")->with("comments")->with("productImages")->with("category")->with("subcategory")->where("category_id",$id)->get();
   }

   public function getFeaturedCategories(): Collection
   {
       return $this->model->orderBy('id', 'desc')->take(3)->get();
   }
   public function AddCategory($attributes)
   {

    $cat=category::find($attributes->id);

    if($cat!=null){
       $cat->category_name=$attributes->category_name;
       $cat->save();
       Alert::success('Category Edited ', 'Success Message');


       return true;
    }


   $category=new category();
   $category->category_name=$attributes->category_name;
   $category->save();
   Alert::success('Category Added ', 'Success Message');
       return true;
   }

   public function DeleteCategory($id)  {
    $this->model->find($id)->delete();
    Alert::success('Category Deleted ', 'Success Message');
       return true;
   }

}
