<?php

namespace App\Repository\Main;


use App\Repository\Interfaces\ShopInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

use Alert;

use App\User;
use App\size;
use App\product;
use App\Comment;
use Stripe\Stripe;
use Stripe\Charge;



use Illuminate\Support\Facades\Mail;
use App\Mail\PaymentDoneEmail;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Auth ;
use Illuminate\Support\Facades\DB;

class ShopRepository implements ShopInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {

    }

   /**
    * @return Collection
    */


    public function getSizes(){
        return size::all();
    }


    public function getProduct($id){

        return product::where("id",$id)->with("sizes")->with("productImages")->with(['comments' => function ($query) {
            $query->take(2)->orderby('updated_at','DESC');
        }])->first();
    }

    public function productAverage($id){
return round(Comment::where("product_id",'=',$id)->avg('product_rating'));

    }



    public function AddComment($request){

   $comment= Comment::create($request->all());

   return response()->json(["comment"=>$comment,"success"=>true],200);
    }

    public function ProcessPayment($attributes){
        Stripe::setApiKey("sk_test_51HwSvoAAirG5o3BvGghPNDsquCtuWEBBfiZ1mfChyOb1o9hxxX6sKvQtV8IbReGdTTYw6B6CuqZH1hPDTvMZETaf001HJ1gSHN");
        $token=$_POST['stripeToken'];
        $charge = Charge::create([
            'amount' =>  $attributes->total,
            'currency' => 'usd',
            'description' => 'udemy course practice selling books',
            'source' => $token,
            'metadata' => ['order_id' => uniqid()],
        ]);
        //order

        $data = array();
        $data['user_id'] = Auth::id();
        $data['payment_id'] = $charge->payment_method;
        $data['paying_amount'] = $charge->amount/100;
        $data['blnc_transection'] = $charge->balance_transaction;
        $data['stripe_order_id'] = $charge->metadata->order_id;
        $data['shipping'] = 8;
        $data['total'] = $attributes->total/100 +8;
        $data['payment_type'] = 1;
        $data['status'] = 0;
        $data['date'] = date('d-m-y');
        $data['month'] = date('F');
        $data['year'] = date('Y');
        $data['status_code'] = mt_rand(100000,999999);
        $order_id = DB::table('orders')->insertGetId($data);

//shipping Adding
        $shipping = array();
        $shipping['order_id'] = $order_id;
        $shipping['ship_name'] = $attributes->ship_name;
        $shipping['ship_phone'] = $attributes->ship_phone;
        $shipping['ship_email'] = $attributes->ship_email;
        $shipping['ship_address'] = $attributes->ship_address;
        $shipping['ship_city'] = $attributes->ship_city;
        DB::table('shippings')->insert($shipping);
//orderDetails

$content = DB::select('select * from users a, products b,carts c where a.id=:id and b.id=c.product_id',['id' =>Auth::user()->id]);
$details = array();
foreach ($content as $row) {
$details['order_id'] = $order_id;
$details['product_id'] = $row->product_id;
$details['product_name'] = $row->product_name;
$details['quantity'] = $row->number;
if($row->discount>0){
    $details['singleprice'] =$row->price - $row->price*$row->discount/100;
    $details['totalprice'] = $row->number*$details['singleprice'];
}
else {
    $details['singleprice'] = $row->price;
    $details['totalprice'] = $row->number*$row->price;
}
DB::table('order_details')->insert($details);

}
if (Session::has('coupon')) {
    Session::forget('coupon');
}

DB::table('carts')->where('user_id',Auth::user()->id)->delete();

Mail::to($shipping['ship_email'])->send(new PaymentDoneEmail());

        return view('web.shop.donecheckout');
    }

}
