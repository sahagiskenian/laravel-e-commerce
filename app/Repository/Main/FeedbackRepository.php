<?php

namespace App\Repository\Main;


use App\Repository\Interfaces\FeedbackInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\feedback;
use Alert;
use Request;
class FeedbackRepository implements FeedbackInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(feedback $model)
    {
        $this->model = $model;
    }

   /**
    * @return Collection
    */
   public function getFeedbacks(): Collection
   {
       return $this->model->all();
   }
   public function AddFeedback(array $attributes)
   {if (Request::is('api*')) {

    $this->model->create($attributes);

    return response(["message"=>"Feedback Added Thank You","status"=>200]);
   }

   else {
    $this->model->create($attributes);
    Alert::success('Feedback Posted', 'Success Message');
    return redirect()->back();
   }

   }

   public function DeleteFeedback($id)  {
       return $this->model->find($id)->delete();
   }

}
