<?php

namespace App\Repository\Main;


use App\Repository\Interfaces\CartInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;



use App\Coupon;
use App\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class CartRepository implements CartInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {

    }

   /**
    * @return Collection
    */
   public function getMyCart()
   {  $user=User::with('carts')->where('id',Auth::id())->first();

       return $user;
       //DB::select('select * from users a, products b,carts c where a.id=:id and b.id=c.product_id',['id' =>Auth::user()->id]);
   }
   public function AddToCart($attributes){
    $search=DB::table('carts')->where('product_id',$attributes->product_id)->where('user_id',$attributes->user_id)->get();

    if(count($search->all())==0){
            $user=DB::table('carts')->insert(
                ['user_id' => $attributes->user_id, 'product_id' => $attributes->product_id]);






            return response()->json(200);
    }
    else {
        return response()->json("",500);
    }

   }

   public function RemoveFromCart($id){
    $user=DB::table('carts')->where("product_id",$id)->where('user_id',Auth::user()->id)->delete();


    Alert::success('Removed From Cart', 'Success Message');



   }

   public function ClearCart(){

    $deleted = DB::delete('delete from carts');

    Alert::success('Cart Cleared', 'Success Message');
   }

   public function CountPrice(){

    $result= $this->getMyCart();
    $sum=0;
    foreach ($result->carts as $product) {
         if($product->product->discount>0)
         {
      $sum += $product->number*($product->product->price - ($product->product->price*$product->product->discount/100 ));
         }
         else {
      $sum += $product->product->price;
         }
    }
    return $sum;
   }
     public function CouponApplied($name) {
$Apply=Coupon::where("coupon_name",$name)->get();

$a= $Apply->toArray() ;
$s=$a[0];
$s=(object) $s;

if(count($Apply->toArray())==1){
    Session::put('coupon',[
        'name' => $s->coupon_name,
        'discount' => $s->discount
        ]);
    return redirect()->back();
}
else {

    return redirect()->back();
}


}

}
