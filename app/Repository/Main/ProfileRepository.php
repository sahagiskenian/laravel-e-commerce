<?php

namespace App\Repository\Main;


use App\Repository\Interfaces\ProfileInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Auth;
use Alert;
use DB;
use Hash;
use App\order;
use Request;
class ProfileRepository implements ProfileInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

   /**
    * @return Collection
    */
   public function changePassword($attributes)
   {

       if (Request::is('api*')) {
        $this->model->where('id', auth('api')->user()->id )->update(['password'=>Hash::make($attributes['password'])]);
return response("Success",200);
       }
       $this->model->where('id',Auth::user()->id)->update(['password'=>Hash::make($attributes['password'])]);
       Alert::success('Password Changed Succesfuly');
       return Redirect()->route('profile');
   }
   public function checkOrder($attributes)
   {
    $code = $attributes->status_code;

            $order = DB::table('orders')->where('status_code',$code)->first();
           switch ($order->status) {
               case 0:
                $notification=array(
                    'messege'=>'Youre order is in our waiting list',
                    'alert-type'=>'error'
                     );
                   break;
              case 1:
                    $notification=array(
                        'messege'=>'Youre order is Accepted and in our waiting list',
                        'alert-type'=>'success'
                         );
                       break;
              case 2:
                        $notification=array(
                            'messege'=>'Youre order Processing and will arive to you soon',
                            'alert-type'=>'success'
                             );
                           break;
              case 3:
                            $notification=array(
                                'messege'=>'Youre order has arrived to you !',
                                'alert-type'=>'success'
                                 );
                               break;

                                   case 4:
                                    $notification=array(
                                        'messege'=>'Youre order has been refused !',
                                        'alert-type'=>'error'
                                         );
                                       break;
               default:
               $notification=array(
                'messege'=>'There is no order with this code',
                'alert-type'=>'error'
                 );
                   break;
           }


                     return Redirect()->back()->with($notification);

   }

   public function returnOrder($id) {
    order::where('id',$id)->update(['return_order'=>1]);
    $notification=array(
        'messege'=>'Youre order is pending in our return list',
        'alert-type'=>'success'
         );
         return Redirect()->route('profile')->with($notification);
   }

}
