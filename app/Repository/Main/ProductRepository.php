<?php

namespace App\Repository\Main;


use App\Repository\Interfaces\ProductInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\product;
use App\Comment;
use Alert;
class ProductRepository implements ProductInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(product $model)
    {
        $this->model = $model;
    }

   /**
    * @return Collection
    */
   public function getProducts(): Collection
   {
       return $this->model->with('productImages')->with('sizes')->with('category')->with('subcategory')->get();
   }

   public function getProduct($id)
   {
       return $this->model->with('productImages')->with("comments")->with('sizes')->with('category')->with('subcategory')->where("id","=",$id)->first();
   }
public function getFilteredProducts($attributes){
    if($attributes->subcategoryId == "all"&& $attributes->sizeId == "all"   ) {
        $response=product::where("quantity",">",5)->where("category_id",$attributes->category_id)->paginate(6);
      }

      else {
        $response=product::where("quantity",">",5)->where("category_id",$attributes->category_id);
    if($attributes->sizeId!= "all" ){

        $sizeId=$attributes->sizeId;
        $response   = $response->whereHas('sizes', function($q) use($sizeId) {
            $q->where('size_id','=', $sizeId);
        });
    }

    if($attributes->subcategoryId!= "all" ){

        $subcategoryId=$attributes->subcategoryId;
        $response=$response->where('subcategory_id','=',$subcategoryId);
    }
    $response=$response->paginate(6);

}
return $response;
}
public function getProductsCategorized($id){
  return  product::with("productImages")->where("quantity",">",5)->where('category_id','=',$id)->paginate(6);
}
public function getLastProducts(){
   return  product::with("productImages")->with('category')->with("subcategory")->with("sizes")->with("comments")->where("quantity",">",5)->orderBy('id', 'desc')->take(6)->get();

}

public function getRelatedProducts($category_id,$id){
    return  product::with("productImages")->with("comments")->where("id","!=",$id)->where("category_id","=",$category_id)->where("quantity",">",5)->orderBy('id', 'desc')->take(6)->get();

 }
 public function getProductAvgRate($id){

return Comment::where("product_id",$id)->avg("product_rating");
 }

 public function getProductRateCount($id){

    return Comment::where("product_id",$id)->count();

 }


}
