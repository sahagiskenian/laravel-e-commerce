<?php

namespace App\Repository\Main;


use App\Repository\Interfaces\UserInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;
use Alert;
use Auth;
use App\User;
use App\product;
use Request;
use Hash;
class UserRepository implements UserInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {

    }

   /**
    * @return Collection
    */
   public function getUsers()
   {
    $users=User::where("isAdmin",0)->get();
    return view('admin.users.users')
           ->with("users",$users);
   }
  public function getAdmins(){
    $users=User::where("isAdmin",1)->get();
    return view('admin.users.admins')
           ->with("users",$users);
  }
  public function addAdmin($attributes){
    $userr=User::find($attributes->id);
    if($userr == null ){
     $user = User::create([

         "name"    => $attributes->name,
         "email"  => $attributes->email,
         "password"  =>   Hash::make($attributes['password']),



     ]);
     $user->isAdmin=1;
     $user->adminType=$attributes->adminType;
     $user->save();
     Alert::success(  'Added Admin Succesfully');
     return redirect()->route("admins");
 }
 $userr->name=$attributes->name;
 $userr->email=$attributes->email;
 $userr->adminType=$attributes->adminType;
 $userr->password=Hash::make($attributes['password']);
 $userr->save();
 Alert::success(  'Edited Admin Succesfully ');
 return  redirect()->route("admins");
  }



   public function RemoveAdmin($id){
    $user=User::find($id)->delete();
   Alert::success(  'Admin Removed Succesfully ');
     return  Redirect()->route("admins");

   }

   public function RemoveUser($id){

    $user=User::find($id)->delete();
    Alert::success(  'User Removed Succesfully ');
      return  Redirect()->route("users");
   }





}
