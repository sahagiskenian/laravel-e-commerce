<?php

namespace App\Repository\Main;


use App\Repository\Interfaces\AdminReportsInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use DB;
use Alert;
use Auth;
use App\order;
use App\category;
use App\User;
use App\subcategory;
use Session;
class AdminReportsRepository implements AdminReportsInterface
{


    protected $model;













    public function __construct()
    {

    }
    public function todayOrders(){
        $date = date('d-m-y');
        $today =order::where('date',$date)->sum('total');
        return $today;
    }

    public function monthOrders(){
        $month = date('F');
        $month = order::where('month',$month)->sum('total');
        return $month;
    }

    public function yearOrders(){
        $year = date('Y');
        $year = order::where('year',$year)->sum('total');

        return $year;
    }

    public function todayDelivered(){
        $date = date('d-m-y');
        $delevery = order::where('date',$date)->where('status',3)->sum('total');
        return $delevery;
    }

    public function orderReturned(){
        $return = order::where('return_order',2)->sum('total');
        return $return;
    }

    public function usersNumber(){
        $user = count(User::all());
        return $user;
    }

    public function categoriesNumber(){
        $category = count(category::all());
        return $category;
    }

    public function subcategoriesNumber(){
        $subcategory = count(subcategory::all());
        return $subcategory;
    }

}
