<?php

namespace App\Repository\Main;


use App\Repository\Interfaces\AuthInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Alert;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator as FacadesValidator;

class AuthRepository implements AuthInterface
{


    protected $model;


    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function Register(array $data){
        $validator = FacadesValidator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails())
        {
            return response(['message'=>$validator->errors()->all(),"success"=>false], 200);
        }

        $user = User::where('email',  $data['email'])->first();
        if ($user) {

            $response = ["message" => "This Email Exist","success"=>false];
            return response($response, 200);

         }

        else {
 $user=User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $array=[
           'token'=>$token,
           'user'=> $user,
            "success"=>true,
           'Message'=>"User Added Succesfully"

         ];

        return response()->json($array,200);
        }
    }

    public function Login(array $data) {
        $validator = FacadesValidator::make($data, [
            'email' => 'required|string|max:255',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 401);
        }

        $user = User::where('email',  $data['email'])->first();

        if ($user) {
            if (Hash::check( $data['password'], $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = [
                    'user'=>$user,
                    'token' => $token,
                    'success'=>true];
                return response($response, 200);
            } else {
                $response = ["message" => "Password mismatch","success"=>false];
                return response($response, 200);
            }
        } else {
            $response = ["message" =>'User does not exist',"success"=>false];
            return response($response, 200);
        }

    }


    public function logout ($request) {

        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }
}
