<?php

namespace App\Repository\Main;


use App\Repository\Interfaces\WishlistInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

use Alert;

use App\User;
use App\product;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Request;
class WishlistRepository implements WishlistInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct()
    {

    }

   /**
    * @return Collection
    */
   public function getMyWishList()
   {
    if (Request::is('api*')) {
        return   User::with('products')->where('id',auth('api')->user()->id)->first();
    }
    else {
        return   User::with('products')->where('id',Auth::user()->id)->first();
    }
   }



   public function AddToMyWishlist($attributes){
    $search=DB::table('product_user')->where('product_id',$attributes->product_id)->where('user_id',$attributes->user_id)->get();

    if(count($search->all())==0){
            $user=User::find($attributes->user_id);


            $user->products()->attach($attributes->product_id);

            $user->save();
            $product=product::find($attributes->product_id);
            $array=[
                'product'=>$product,
                'status'=>200,
                'message'=>'Added Product to your Wishlist Sucessfuly',
                'success'=>true
            ];
            return response()->json($array,200);
    }
    else {
        $array=[
            'message'=>'exist in your wishlist',
            'success'=>false
        ];
        return response()->json("error",403);
    }

   }

   public function RemoveFromWishlist($product_id){
    if (Request::is('api*')) {
        $user=User::find(auth('api')->user()->id);
   $product=product::find($product_id);
    $user->products()->detach($product_id);

return $product;

    }
    else {
        $user=User::find(Auth::user()->id);

        $user->products()->detach($product_id);



        return response()->json($user,200);
    }

   }

   public function WishlistToCart($attributes){


    $transfer= DB::table('carts')->insert(
        ['user_id' => $attributes->user_id, 'product_id' => $attributes->product_id]);

        return response()->json($transfer,200);
   }





}
