<?php

namespace App\Repository\Main;


use App\Repository\Interfaces\SubCategoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\subcategory;
use RealRashid\SweetAlert\Facades\Alert;

class SubCategoryRepository implements SubCategoryInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(subcategory $model)
    {
        $this->model = $model;
    }

   /**
    * @return Collection
    */
   public function getSubCategories($id): Collection
   {
       return $this->model->with('category')->where('category_id',$id)->get();
   }
   public function AddSubCategory($attributes)
   {

    $cat=subcategory::find($attributes->id);

    if($cat!=null){
       $cat->subcategory_name=$attributes->subcategory_name;
       $cat->category_id=$attributes->category_id;
       $cat->save();
       Alert::success('SubCategory Edited ', 'Success Message');


       return true;
    }


   $category=new subcategory();
   $category->subcategory_name=$attributes->subcategory_name;
   $category->category_id=$attributes->category_id;
   $category->save();
   Alert::success(' SubCategory Added ', 'Success Message');
       return true;
   }

   public function DeleteSubCategory($id)  {
    $this->model->find($id)->delete();
    Alert::success('SubCategory Deleted ', 'Success Message');
       return true;
   }

}
