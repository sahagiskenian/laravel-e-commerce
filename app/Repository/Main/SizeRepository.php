<?php

namespace App\Repository\Main;


use App\Repository\Interfaces\SizeInterface;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\size;
use Alert;
class SizeRepository implements SizeInterface
{

   /**
    * UserRepository constructor.
    *
    * @param User $model
    */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(size $model)
    {
        $this->model = $model;
    }

   /**
    * @return Collection
    */
   public function getSizes()
   {
       return $this->model->all();
   }
   public function AddSize($attributes)
   {

    $cat=size::find($attributes->id);

    if($cat!=null){
       $cat->size_name=$attributes->size_name;
       $cat->save();
       Alert::success('Size Edited ', 'Success Message');


       return true;
    }


   $category=new size();
   $category->size_name=$attributes->size_name;
   $category->save();
   Alert::success(' Size Added ', 'Success Message');
       return true;
   }

   public function DeleteSize($id)  {
    $this->model->find($id)->delete();
    Alert::success('Size Deleted ', 'Success Message');
       return true;
   }

}
