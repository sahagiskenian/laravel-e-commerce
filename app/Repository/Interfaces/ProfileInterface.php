<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface ProfileInterface
{

   public function changePassword($attributes);

   public function checkOrder($attributes);

   public function returnOrder($id);


}
