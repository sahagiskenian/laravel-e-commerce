<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface FeedbackInterface
{

   public function getFeedbacks(): Collection;

   /**
    * @param $id
    * @return Model
    */
    public function AddFeedback(array $attributes);

    public function DeleteFeedback($id);
}
