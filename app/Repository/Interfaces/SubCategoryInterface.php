<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface SubCategoryInterface
{

   public function getSubCategories($id): Collection;

   /**
    * @param $id
    * @return Model
    */
    public function AddSubCategory($attributes) ;

    public function DeleteSubCategory($id);
}
