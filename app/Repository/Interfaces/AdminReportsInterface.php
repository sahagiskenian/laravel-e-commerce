<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface AdminReportsInterface
{

   public function todayOrders();

   public function monthOrders();

   public function yearOrders();

   public function todayDelivered();

   public function orderReturned();

   public function usersNumber();

   public function categoriesNumber();

   public function subcategoriesNumber();
}
