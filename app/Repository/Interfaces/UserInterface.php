<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface UserInterface
{

   public function getAdmins();

   public function getUsers();

   /**
    * @param $id
    * @return Model
    */
     public function AddAdmin($attributes);



    public function RemoveAdmin($id);

    public function RemoveUser($id);

}
