<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface AuthInterface
{

   public function Register(array $attributes);

   /**
    * @param $id
    * @return Model
    */
    public function Login(array $attributes);

    public function logout($request);
}
