<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface ProductInterface
{

   public function getProducts(): Collection;

   public function getProductsCategorized($id);

   public function getfilteredProducts($attributes);

   public function getLastProducts();

   public function getRelatedProducts($subcategory_id,$id);

   public function getProduct($id);

   public function getProductAvgRate($id);

   public function getProductRateCount($id);
}
