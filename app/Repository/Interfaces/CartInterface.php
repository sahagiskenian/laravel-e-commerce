<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface CartInterface
{

   public function getMyCart();

   /**
    * @param $id
    * @return Model
    */
    public function AddToCart($attributes);

    public function RemoveFromCart($id);

    public function ClearCart();

    public function CountPrice();

    public function CouponApplied($id);
}
