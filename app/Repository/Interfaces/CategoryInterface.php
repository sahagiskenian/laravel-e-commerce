<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface CategoryInterface
{

   public function getCategories();

   public function categoryProducts($id);

   public function getFeaturedCategories(): Collection;

   /**
    * @param $id
    * @return Model
    */
    public function AddCategory($attributes) ;

    public function DeleteCategory($id);
}
