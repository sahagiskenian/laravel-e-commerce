<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface ShopInterface
{

   public function getSizes();

   /**
    * @param $id
    * @return Model
    */
    public function getProduct($id);

    public function productAverage($id);


    public function AddComment($request);

    public function ProcessPayment($attributes);


}
