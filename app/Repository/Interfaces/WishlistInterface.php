<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface WishlistInterface
{

   public function getMyWishList();


   /**
    * @param $id
    * @return Model
    */
    public function AddToMyWishlist($attributes);



    public function RemoveFromWishlist($id);

    public function WishlistToCart($id);

}
