<?php


namespace App\Repository\Interfaces;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
/**
* Interface EloquentRepositoryInterface
* @package App\Repositories
*/
interface SizeInterface
{

   public function getSizes();

   /**
    * @param $id
    * @return Model
    */
    public function AddSize($attributes) ;

    public function DeleteSize($id);
}
