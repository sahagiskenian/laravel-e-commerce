<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    //
    protected $fillable = [
        'product_name','product_description','quantity','main_image','price','discount','hide','top_rated'
        ,'product_code','category_id','subcategory_id'
    ];
    public function category()
    {
        return $this->belongsTo('App\category');
    }
    public function subcategory()
    {
        return $this->belongsTo('App\subcategory');
    }


    public function sizes()
    {
        return $this->belongsToMany('App\size');
    }
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
    public function productImages()
    {
            return $this->hasMany('App\productImage');
    }
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
    public function orderdetails()
    {
        return $this->hasMany('App\orderDetail');
    }
}
