<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class size extends Model
{
    //
    protected $fillable = [
        'size_name'
    ];

    public function products()
    {
        return $this->belongsToMany('App\product');
    }
}
