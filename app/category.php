<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    //
    protected $fillable = [
        'category_name'
    ];


    public function subcategories()
    {
            return $this->hasMany('App\subcategory');
    }
    public function products()
    {
            return $this->hasMany('App\product');
    }

}
