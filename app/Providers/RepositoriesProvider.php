<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\Interfaces\FeedbackInterface;
use App\Repository\Main\FeedbackRepository;
use App\Repository\Interfaces\CategoryInterface;
use App\Repository\Main\CategoryRepository;
use App\Repository\Interfaces\SubCategoryInterface;
use App\Repository\Main\SubCategoryRepository;
use App\Repository\Interfaces\SizeInterface;
use App\Repository\Main\SizeRepository;
use App\Repository\Interfaces\CartInterface;
use App\Repository\Main\CartRepository;
use App\Repository\Interfaces\WishlistInterface;
use App\Repository\Main\WishlistRepository;
use App\Repository\Interfaces\ShopInterface;
use App\Repository\Main\ShopRepository;
use App\Repository\Interfaces\ProductInterface;
use App\Repository\Main\ProductRepository;
use App\Repository\Interfaces\AuthInterface;
use App\Repository\Main\AuthRepository;
use App\Repository\Interfaces\UserInterface;
use App\Repository\Main\UserRepository;
use App\Repository\Interfaces\AdminReportsInterface;
use App\Repository\Main\AdminReportsRepository;
use App\Repository\Interfaces\ProfileInterface;
use App\Repository\Main\ProfileRepository;
class RepositoriesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(FeedbackInterface::class, FeedbackRepository::class);
        $this->app->bind(CategoryInterface::class, CategoryRepository::class);
        $this->app->bind(SubCategoryInterface::class, SubCategoryRepository::class);
        $this->app->bind(SizeInterface::class, SizeRepository::class);
        $this->app->bind(CartInterface::class, CartRepository::class);
        $this->app->bind(WishlistInterface::class, WishlistRepository::class);
        $this->app->bind(ShopInterface::class, ShopRepository::class);
        $this->app->bind(ProductInterface::class, ProductRepository::class);
        $this->app->bind(AuthInterface::class, AuthRepository::class);
        $this->app->bind(UserInterface::class, UserRepository::class);
        $this->app->bind(AdminReportsInterface::class, AdminReportsRepository::class);
        $this->app->bind(ProfileInterface::class, ProfileRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
