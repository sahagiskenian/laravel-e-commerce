<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $fillable = [
        'comment_name','comment_email','comment_description','product_rating','product_id'
    ];

    public function product()
    {
        return $this->belongsTo('App\product');
    }
}
