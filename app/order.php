<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    //


    public function orderdetails()
    {
        return $this->hasMany('App\orderDetail');
    }
    public function shippings()
    {
        return $this->hasMany('App\shipping');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
