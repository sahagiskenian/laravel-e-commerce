<?php

use App\category;
use App\product;
use App\productImage;
use App\productsize;
use App\subcategory;
use Illuminate\Database\Seeder;
use database\seeds\CouponSeeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
     //    factory( category::class,4)->create();
     factory( productsize::class,100)->create();
    }
}
