<?php

use Illuminate\Database\Seeder;

class CouponSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('coupons')->insert([
            'coupon_name' => Str::random(10),
            'discount' => 15,

        ]);
        DB::table('coupons')->insert([
            'coupon_name' => Str::random(10),
            'discount' => 20,

        ]);
    }
}
