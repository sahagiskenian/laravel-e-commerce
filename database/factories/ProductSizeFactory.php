<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\productsize;
use Faker\Generator as Faker;

$factory->define(productsize::class, function (Faker $faker) {
    return [
        //
         "product_id"=>$faker->numberBetween(22,72),
         "size_id"=>$faker->randomElement($array = array (1,3,4,5,6,7))
    ];
});
