<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\productImage;
use Faker\Generator as Faker;

$factory->define(productImage::class, function (Faker $faker) {
    return [
        //
        "product_id"=>$faker->numberBetween(21,72),
        "product_image"=>$faker->imageUrl(640,480)
    ];
});
