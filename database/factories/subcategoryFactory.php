<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\subcategory;
use Faker\Generator as Faker;

$factory->define(subcategory::class, function (Faker $faker) {
    return [
        //
        "subcategory_name"=>$faker->name(),
        "category_id"=>$faker->randomElement($array = array (1,6,14,19,20,21,22))
    ];
});
