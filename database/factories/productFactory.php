<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\product;
use Faker\Generator as Faker;

$factory->define(product::class, function (Faker $faker) {
    return [
        //
        "product_name"=>$faker->name,
        "product_description"=>$faker->paragraph($nbSentences = 3, $variableNbSentences = true) ,
         "quantity"=>$faker->numberBetween(10,100),
         "main_image"=>$faker->imageUrl($width = 640, $height = 480),
         "price"=>$faker->numberBetween(5000,50000),
         "discount"=>$faker->numberBetween(10,70),
         "product_code"=>$faker->unique()->numberBetween(100,2000),
         "top_rated"=>$faker->numberBetween(0,1),
         "category_id"=>$faker->randomElement($array = array (1,6,14,19,20,21,22))
    ];
});
