<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->json("addresses")->nullable();
            $table->json("emails")->nullable();
            $table->json("numbers")->nullable();
            $table->string("facebook_url")->nullable();
            $table->string("aboutus_stats1")->nullable();
            $table->string("aboutus_stats2")->nullable();
            $table->string("aboutus_stats3")->nullable();
            $table->string("aboutus_stats4")->nullable();
            $table->string("aboutus_image")->nullable();
            $table->string("aboutus_title")->nullable();
            $table->string("aboutus_description")->nullable();
            $table->string("aboutus_num1")->nullable();
            $table->string("aboutus_num2")->nullable();
            $table->string("aboutus_num3")->nullable();
            $table->string("aboutus_num4")->nullable();
            $table->string("logo")->nullable();
            $table->string("slider1_text")->nullable();
            $table->string("slider2_text")->nullable();
            $table->string("slider3_text")->nullable();
            $table->string("slider_image1")->nullable();
            $table->string("slider_image2")->nullable();
            $table->string("slider_image3")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
